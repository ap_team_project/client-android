package com.APproject.apgram;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.APproject.apgram.client.Chats.ChatType;
import com.APproject.apgram.client.SimpleData;
import com.APproject.apgram.client.net.CreateChannelRequest;
import com.APproject.apgram.client.net.CreateGroupRequest;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddChatActivity extends AppCompatActivity {
    static String[] chatTypes = {"group","channel"};

    EditText chatName;
    Spinner chatTypeSpinner;

    CircleImageView chatIcon;

    ListView membersList;
    ArrayAdapter<String> memberListAdapter;

    EditText newMemberUsername;



    String iconPath = null;
    ArrayList<String> users = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_chat);
        //finding views
        chatName = findViewById(R.id.name);
        chatTypeSpinner = findViewById(R.id.spinner);
        chatIcon = findViewById(R.id.icon);
        newMemberUsername = findViewById(R.id.newMemberUsername);
        membersList = findViewById(R.id.memberList);


        //set the views
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,chatTypes);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        chatTypeSpinner.setAdapter(spinnerAdapter);
        memberListAdapter = new ArrayAdapter<>(this,R.layout.member_list_item,users);
        membersList.setAdapter(memberListAdapter);
    }





    //buttons listener
    public void setIcon(View view){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }
    public void addChat(View view){
        String type = (String) chatTypeSpinner.getSelectedItem();
        String myID = SimpleData.getInstance().myID;
        String name= chatName.getText().toString();
        if(type == "group"){
            new CreateGroupRequest(myID, name, users);
        }else if(type == "channel"){
            new CreateChannelRequest(myID, name, users);
        }
        finish();
    }
    public void addNewMember(View view){
        String username = newMemberUsername.getText().toString();
        users.add(username);
        newMemberUsername.setText("");
        memberListAdapter.notifyDataSetChanged();
    }

    //result of choosing file
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode==Activity.RESULT_OK && data!=null){
            iconPath = data.getData().getPath();
            chatIcon.setImageURI(data.getData());
        }
    }
}
