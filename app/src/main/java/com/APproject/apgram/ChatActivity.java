package com.APproject.apgram;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.APproject.apgram.GraphicElements.MessageRecyclerAdapter;
import com.APproject.apgram.client.Chats.Access;
import com.APproject.apgram.client.Chats.Chat;
import com.APproject.apgram.client.Chats.ChatType;
import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.Files.FileManager;
import com.APproject.apgram.client.Messages.Message;
import com.APproject.apgram.client.GraphicUpdater.UpdateManager;
import com.APproject.apgram.client.User.User;
import com.APproject.apgram.client.Util;
import com.APproject.apgram.client.net.MessageRequest;
import com.APproject.apgram.client.net.RemoveChannelRequest;
import com.APproject.apgram.client.net.RemoveGroupRequest;
import com.APproject.apgram.client.net.SendFileRequest;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity {
    public static String tag = "ChatActivity";
    static final int PICK_IMAGE = 1;
    static final int PICK_FILE = 2;
    static final int PICK_VIDEO = 3;
    static final int PICK_MUSIC = 4;

    String chatID;
    Chat chat;

    TextView chatName;
    CircleImageView icon;

    RecyclerView recycler;
    MessageRecyclerAdapter messageRecyclerAdapter;

    ArrayList<Message> messages;

    EditText messageText;


    ImageButton sendFileButton;
    ImageButton sendImageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        chatID = getIntent().getStringExtra("chatID");
        UpdateManager.getInstance().setChatActivityUpdater(this);
        //setting the toolbar
        Toolbar toolbar = findViewById(R.id.chat_toolbar);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seeChatDetail();
            }
        });
        icon = findViewById(R.id.chatIcon);
        chatName = findViewById(R.id.name);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
        sendFileButton = findViewById(R.id.sendFileButton);
        sendFileButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent();
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                Log.i(tag, "start picking video");
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_VIDEO);
                return false;
            }
        });
        sendImageButton = findViewById(R.id.sendImageButton);
        sendImageButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent();
                intent.setType("audio/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                Log.i(tag, "start picking music");
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_MUSIC);
                return false;
            }
        });
        messageText = findViewById(R.id.messageText);
        chat = DatabaseManager.getInstance().getChat(chatID);
        if(chat.getChatType() == ChatType.PRIVATE){
            String userID = chatID.substring(1);
            User user = null;
            user = DatabaseManager.getInstance().getUser(userID);
            chatName.setText(user.getUserName());
            icon.setImageBitmap(BitmapFactory.decodeStream(FileManager.getInstance().getIcon(user.getID())));
        }
        else {
        chatName.setText(chat.getChatName());
        icon.setImageBitmap(BitmapFactory.decodeStream(FileManager.getInstance().getIcon(chatID)));
        }
        if(chat.getChatType() == ChatType.CHANNEL){
            if(chat.getMyAccess() == Access.NORMAL)
                canSendMessage(false);
            canSendMessage(true);
        }
        else
            canSendMessage(true);
        messages = DatabaseManager.getInstance().getChatMassages(chatID);
        recycler = findViewById(R.id.recyclerView);
        messageRecyclerAdapter = new MessageRecyclerAdapter(this, messages,chatID);
        recycler.setAdapter(messageRecyclerAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(linearLayoutManager);
        gotoLastMessage();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        UpdateManager.getInstance().removeChatActivity();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    public String getChatID(){
        return chatID;
    }
    //back button click listener
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void canSendMessage(boolean value){
        ConstraintLayout layout = findViewById(R.id.sendMessageLayout);
        layout.setVisibility(View.INVISIBLE);
    }
    public void addMessage(Message message){
        messages.add(message);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                messageRecyclerAdapter.notifyItemInserted(messages.size());
            }
        });
        Toast.makeText(this, "new message", Toast.LENGTH_SHORT).show();
    }
    public void setDownloaded(final String messageID){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                messageRecyclerAdapter.setDownloaded(messageID);
            }
        });

    }
    private void gotoLastMessage(){
        recycler.scrollToPosition(messageRecyclerAdapter.getItemCount()-1);
    }

    public void seeChatDetail(){
        String type = null;
        String id = chatID;
        if(chat.getChatType() == ChatType.PRIVATE){
            type = "user";
            id = chatID.substring(1);
        }
        else if(chat.getChatType() == ChatType.CHANNEL){
            type = "channel";
        }
        else if(chat.getChatType() == ChatType.GROUP){
            type = "group";
        }
        Intent intent = new Intent(this, SeeInfoActivity.class);
        intent.setType(type);
        intent.putExtra("id",id);
        startActivity(intent);
    }

    //buttons click listener
    public void sendMessage(View view){
        int viewID = view.getId();
        if(viewID == R.id.sendTextButton){
            String text = messageText.getText().toString();
            if(text.equals(""))
                return;
            new MessageRequest(text, chat.getID());
            messageText.setText("");
            return;
        }
        if(!messageText.getText().toString().equals(""))
                return;
        if(viewID == R.id.sendImageButton){
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            Log.i(tag, "start picking image");
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
        }
        else {
            Intent intent = new Intent();
            intent.setType("*/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            Log.i(tag, "start picking file");
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_FILE);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(!(resultCode == Activity.RESULT_OK))
            return;
        String type = "";
        if(requestCode == PICK_IMAGE){
            type = "IMAGE";
        }
        else if(requestCode == PICK_FILE){
            type = "FILE";
        }
        else if(requestCode == PICK_VIDEO){
            type = "VIDEO";
        }
        else if(requestCode == PICK_MUSIC){
            type = "MUSIC";
        }
        new SendFileRequest(chatID,data.getData().getPath(), type);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.chatDeleteItem){
            if(chat.getChatType() == ChatType.GROUP){
                new RemoveGroupRequest(chatID);
            }
            else if(chat.getChatType() == ChatType.CHANNEL){
                new RemoveChannelRequest(chatID);
            }
        }
        else if(item.getItemId() == R.id.infoItem){
            Intent intent = new Intent(this, SeeInfoActivity.class);
            String type = null;
            switch (chat.getChatType()){
                case PRIVATE:
                    type = "user";
                    break;
                case GROUP:
                    type = "group";
                    break;
                case CHANNEL:
                    type = "channel";
                    break;
            }
            intent.setType(type);
            String chatID = chat.getID();
            if(chat.getChatType() == ChatType.PRIVATE){
                chatID = Util.getUserIdFromPrivateChat(chatID);
            }
            intent.putExtra("id",chatID);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
