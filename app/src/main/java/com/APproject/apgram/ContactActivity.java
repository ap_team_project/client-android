package com.APproject.apgram;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.User.User;
import com.APproject.apgram.client.net.AddContactRequest;

import java.util.ArrayList;

public class ContactActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    EditText newContact;
    ListView contactList;

    ArrayList<User> contacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        newContact =findViewById(R.id.newContactUsername);
        contactList = findViewById(R.id.contactsListView);
        contacts = DatabaseManager.getInstance().getContacts();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.member_list_item,getUsersUsername());
        contactList.setAdapter(adapter);
        contactList.setOnItemClickListener(this);
    }

    ArrayList<String> getUsersUsername(){
        ArrayList<String> result = new ArrayList<>();
        for(User user:contacts){
            result.add(user.getUserName());
        }
        return result;
    }

    public void addContact(View view){
        String newContactUsername = newContact.getText().toString();
        new AddContactRequest(newContactUsername);
        newContact.setText("");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        User user = contacts.get(position);
        Intent intent = new Intent(this, SeeInfoActivity.class);
        intent.setType("user");
        intent.putExtra("id",user.getID());
        startActivity(intent);
        finish();
    }
}
