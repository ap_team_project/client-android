package com.APproject.apgram.GraphicElements;

import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.APproject.apgram.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatElement extends RecyclerView.ViewHolder {
    RelativeLayout layout;
//    View background;

    CircleImageView icon;
    TextView chatName;
    TextView lastMessage;

    TextView lastActivityTime;
    ImageView hasNewMessage;
    TextView newMessageCount;

    View separator;

    public ChatElement(@NonNull View itemView) {
        super(itemView);
        layout = itemView.findViewById(R.id.layout);
        icon = itemView.findViewById(R.id.chatIcon);
        chatName = itemView.findViewById(R.id.name);
        lastMessage = itemView.findViewById(R.id.lastMessage);
        lastActivityTime = itemView.findViewById(R.id.lastActivityTime);
        hasNewMessage = itemView.findViewById(R.id.hasNewMessage);
        newMessageCount = itemView.findViewById(R.id.newMessageCount);
//        background = itemView.findViewById(R.id.backgroundColorView);
        separator = itemView.findViewById(R.id.separator);
    }

}
