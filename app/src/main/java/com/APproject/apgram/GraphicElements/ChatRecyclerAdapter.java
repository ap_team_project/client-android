package com.APproject.apgram.GraphicElements;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.APproject.apgram.ChatActivity;
import com.APproject.apgram.R;
import com.APproject.apgram.client.Chats.Chat;
import com.APproject.apgram.client.Chats.ChatManager;
import com.APproject.apgram.client.Files.FileManager;

import java.io.InputStream;

public class ChatRecyclerAdapter extends RecyclerView.Adapter<ChatElement> {
    private ChatManager chatManager;
    private Context context;


    public ChatRecyclerAdapter(Context context, ChatManager chatManager) {
        this.chatManager = chatManager;
        this.context = context;
    }

    @NonNull
    @Override
    public ChatElement onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_element,parent, true);
        return new ChatElement(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatElement holder, int position) {
        final Chat chat = chatManager.get(position);

        //set the chat icon
        InputStream icon =FileManager.getInstance().getIcon(chat.getID());
        if (icon == null){
            holder.icon.setImageResource(R.drawable.chat_default_icon);
        }else {
            holder.icon.setImageBitmap(BitmapFactory.decodeStream(icon));
        }

        //set chat name
        holder.chatName.setText(chat.getChatName());

        //set last message
        holder.lastMessage.setText(chatManager.getLastMessageString(position));

        //set last activity time
        holder.lastActivityTime.setText(chatManager.getLastActivityTime(position));

        //set new messages count
        int newMessages = chat.getNewMessageCount();
        if(newMessages == 0){
            holder.hasNewMessage.setImageIcon(Icon.createWithResource(context, R.drawable.no_new_message));
            holder.newMessageCount.setVisibility(View.INVISIBLE);
        }
        else {
            if(newMessages > 99)
                newMessages = 99;
            holder.hasNewMessage.setImageIcon(Icon.createWithResource(context, R.drawable.have_message));
            holder.newMessageCount.setText(newMessages);
            holder.newMessageCount.setVisibility(View.VISIBLE);
        }

        //set the Listener

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("chatID",chat.getID());
            }
        });

    }



    @Override
    public int getItemCount() {
        return chatManager.size();
    }
}
