package com.APproject.apgram.GraphicElements;

import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.APproject.apgram.R;

public class FileMessageElement extends RecyclerView.ViewHolder {
    LinearLayout layout;
    TextView sender;
    TextView sentTime;
    TextView content;
    CheckBox isDownloaded;

    public FileMessageElement(@NonNull View itemView) {
        super(itemView);
        layout = itemView.findViewById(R.id.layout);
        sender = itemView.findViewById(R.id.sender);
        content =itemView.findViewById(R.id.content);
        isDownloaded = itemView.findViewById(R.id.isDownloaded);
        sentTime = itemView.findViewById(R.id.messageTime);
    }

    public void isDownloaded(boolean value){
        isDownloaded.setChecked(value);
        isDownloaded.setEnabled(false);
    }
    public void isMeSender(boolean value){
        if(value){
            layout.setGravity(Gravity.RIGHT);
        }else {
            layout.setGravity(Gravity.LEFT);
        }
    }

    public void setSender(String senderName){
        if(senderName == null){
            sender.setVisibility(View.INVISIBLE);
            return;
        }
        sender.setText(senderName);
    }
}
