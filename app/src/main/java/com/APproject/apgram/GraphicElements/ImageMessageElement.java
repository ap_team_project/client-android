package com.APproject.apgram.GraphicElements;

import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.APproject.apgram.R;

public class ImageMessageElement extends RecyclerView.ViewHolder {
    LinearLayout layout;
    TextView sender;
    TextView sentTime;
    ImageView image;
    ImageButton isDownloaded;


    public ImageMessageElement(@NonNull View itemView) {
        super(itemView);
        layout = itemView.findViewById(R.id.layout);
        sender = itemView.findViewById(R.id.sender);
        image = itemView.findViewById(R.id.image);
        sentTime = itemView.findViewById(R.id.messageTime);
        isDownloaded = itemView.findViewById(R.id.isDownloaded);
    }

    public void isMeSender(boolean value){
        if(value){
            layout.setGravity(Gravity.RIGHT);
        }else {
            layout.setGravity(Gravity.LEFT);
        }
    }

    public void isDownloaded(boolean value){
        if(value){
            isDownloaded.setVisibility(View.INVISIBLE);
        }
    }

    public void setSender(String senderName){
        if(senderName == null){
            sender.setVisibility(View.INVISIBLE);
            return;
        }
        sender.setText(senderName);
    }
}
