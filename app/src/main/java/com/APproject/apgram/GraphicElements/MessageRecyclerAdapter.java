package com.APproject.apgram.GraphicElements;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.APproject.apgram.R;
import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.Files.FileManager;
import com.APproject.apgram.client.Messages.Message;
import com.APproject.apgram.client.Messages.MessageType;
import com.APproject.apgram.client.SimpleData;
import com.APproject.apgram.client.User.User;
import com.APproject.apgram.client.net.DownloadMessageRequest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MessageRecyclerAdapter extends RecyclerView.Adapter {
    public static final int textType = 1;
    public static final int fileType = 2;
    public static final int imageType = 3;
    public static final int videoType = 4;
    public static final int musicType = 5;


    ArrayList<Message> messages;
    Context context;
    String chatID;

    public MessageRecyclerAdapter(Context context, ArrayList<Message> messages, String chatID) {
        this.context = context;
        this.messages = messages;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case textType:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.text_message_element,parent,false);
                return new TextMessageElement(view);
            case fileType:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.file_message_element,parent,false);
                return new FileMessageElement(view);
            case imageType:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_message_element,parent,false);
                return new ImageMessageElement(view);
            case videoType:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_message_element,parent,false);
                return new VideoMessageElement(view);
            case musicType:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.music_message_element,parent,false);
                return new MusicMessageElement(view);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        switch (messages.get(position).getType()){
            case TEXT:
                return textType;
            case FILE:
                return fileType;
            case IMAGE:
                return imageType;
            case VIDEO:
                return videoType;
            case MUSIC:
                return musicType;
        }
        return -1;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final Message message = messages.get(position);
        if(message.getType() == MessageType.TEXT){
            TextMessageElement element = (TextMessageElement)holder;
            //set sender
            String senderID= message.getSender();
            if(senderID != null){
                if(senderID.equals(SimpleData.getInstance().myID)){
                    element.isMeSender(true);
                    element.sender.setText(SimpleData.getInstance().myUserName);
                }
                else {
                    element.isMeSender(false);
                    String senderName = "";
                    User user = DatabaseManager.getInstance().getUser(senderID);
                    if(user != null)
                        senderName = user.getUserName();
                    element.sender.setText(senderName);
                }
            }
            else {
                element.sender.setVisibility(View.INVISIBLE);
            }

            //setContent
            element.content.setText(message.getContent());

            //set time
            element.sentTime.setText(message.getDate());

        }
        else if(message.getType() == MessageType.FILE){
            FileMessageElement element = (FileMessageElement)holder;
            //set sender
            String senderID= message.getSender();
            if(senderID != null){
                if(senderID.equals(SimpleData.getInstance().myID)){
                    element.isMeSender(true);
                    element.sender.setText(SimpleData.getInstance().myUserName);
                }
                else {
                    element.isMeSender(false);
                    String senderName = "";
                    User user = DatabaseManager.getInstance().getUser(senderID);
                    if(user != null)
                        senderName = user.getUserName();
                    element.sender.setText(senderName);
                }
            }
            else {
                element.sender.setVisibility(View.INVISIBLE);
            }


            //set time
            element.sentTime.setText(message.getDate());

            //set content
            element.isDownloaded(message.isDownloaded());

            element.content.setText(message.getContent());

            //set click listener
            element.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Message message = messages.get(position);
                    if(message.isDownloaded()){
                        openFile(message.getId());
                    }
                    else{
                        makeDownloadRequest(message.getId());
                    }
                }
            });

        }
        else if(message.getType() == MessageType.IMAGE){
            ImageMessageElement element = (ImageMessageElement)holder;
            //set sender
            String senderID= message.getSender();
            if(senderID != null){
                if(senderID.equals(SimpleData.getInstance().myID)){
                    element.isMeSender(true);
                    element.sender.setText(SimpleData.getInstance().myUserName);
                }
                else {
                    element.isMeSender(false);
                    String senderName = "";
                    User user = DatabaseManager.getInstance().getUser(senderID);
                    if(user != null)
                        senderName = user.getUserName();
                    element.sender.setText(senderName);
                }
            }
            else {
                element.sender.setVisibility(View.INVISIBLE);
            }

            //set time
            element.sentTime.setText(message.getDate());

            element.isDownloaded(message.isDownloaded());

            //set the listener

            if(message.isDownloaded()){
                element.image.setImageBitmap(BitmapFactory.decodeStream(FileManager.getInstance().getMessageFileStream(message.getId())));
            }
            element.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!message.isDownloaded()){
                        makeDownloadRequest(message.getId());
                    }
                }
            });
        }
        else if(message.getType() == MessageType.VIDEO){
            final VideoMessageElement element = (VideoMessageElement)holder;
            //set sender
            String senderID= message.getSender();
            if(senderID != null){
                if(senderID.equals(SimpleData.getInstance().myID)){
                    element.isMeSender(true);
                    element.sender.setText(SimpleData.getInstance().myUserName);
                }
                else {
                    element.isMeSender(false);
                    String senderName = "";
                    User user = DatabaseManager.getInstance().getUser(senderID);
                    if(user != null)
                        senderName = user.getUserName();
                    element.sender.setText(senderName);
                }
            }
            else {
                element.sender.setVisibility(View.INVISIBLE);
            }


            //set time
            element.sentTime.setText(message.getDate());

            //set is downloaded
            element.isDownloaded(message.isDownloaded());

            // set video uri
            if(message.isDownloaded()){
                Uri uri = Uri.fromFile(FileManager.getInstance().getMessageFile(message.getId()));
                element.setVideoUri(uri);
            }

            //set the listener
            if(!message.isDownloaded()){
                element.layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        makeDownloadRequest(message.getId());
                    }
                });
            }
            else {
                element.startPause.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        element.startPause();
                    }
                });
            }
        }
        else if(message.getType() == MessageType.MUSIC){
            MusicMessageElement element = (MusicMessageElement)holder;
            //set sender
            String senderID= message.getSender();
            if(senderID != null){
                if(senderID.equals(SimpleData.getInstance().myID)){
                    element.isMeSender(true);
                    element.sender.setText(SimpleData.getInstance().myUserName);
                }
                else {
                    element.isMeSender(false);
                    String senderName = "";
                    User user = DatabaseManager.getInstance().getUser(senderID);
                    if(user != null)
                        senderName = user.getUserName();
                    element.sender.setText(senderName);
                }
            }
            else {
                element.sender.setVisibility(View.INVISIBLE);
            }

            //set time
            element.sentTime.setText(message.getDate());

            // set video uri

            if(message.isDownloaded()){
                Uri uri = Uri.fromFile(FileManager.getInstance().getMessageFile(message.getId()));
                try {
                    element.setMusicUri(context,uri);
                } catch (IOException e) {}
            }

            //set is downloaded
            element.isDownloaded(message.isDownloaded());

            //set the listener
            if(!message.isDownloaded()){
                element.layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        makeDownloadRequest(message.getId());
                    }
                });
            }
        }
    }

    public void openFile(String messageID){
        File file = FileManager.getInstance().getMessageFile(messageID);
        if(file == null)
            return;
        Uri uri = Uri.fromFile(file);
        String mimeType = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
        if(mimeType == null)
            mimeType = "*/*";
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri,mimeType);
        context.startActivity(intent);
    }

    public void makeDownloadRequest(String messageID){
        new DownloadMessageRequest(messageID,chatID);
    }

    public void setDownloaded(String messageID){
        for(int i = 0 ; i < messages.size() ; i++){
            Message message = messages.get(i);
            if(message.getId().equals(messageID)){
                message.setDownloaded();
                notifyItemChanged(i);
                return;
            }
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }
}
