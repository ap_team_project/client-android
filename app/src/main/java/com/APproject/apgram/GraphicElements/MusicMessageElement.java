package com.APproject.apgram.GraphicElements;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.APproject.apgram.R;

import org.bson.Document;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class MusicMessageElement extends RecyclerView.ViewHolder {
    LinearLayout layout;
    TextView sender;
    TextView sentTime;
    ImageButton actionButton;
    SeekBar musicSeekBar;

    MediaPlayer musicPlayer = new MediaPlayer();
    Timer timer = new Timer();
    boolean haveUri = false;

    TimerTask playingTask;

    public MusicMessageElement(@NonNull View itemView) {
        super(itemView);
        layout = itemView.findViewById(R.id.layout);
        sender = itemView.findViewById(R.id.sender);
        actionButton = itemView.findViewById(R.id.actionButton);
        sentTime = itemView.findViewById(R.id.messageTime);
        musicSeekBar = itemView.findViewById(R.id.musicSeekBar);
        musicPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        musicSeekBar.setMax(0);

    }

    public void setMusicUri(Context context, Uri uri) throws IOException {
        musicPlayer.setDataSource(context, uri);
        musicPlayer.prepare();
        haveUri = true;
        musicSeekBar.setMax(musicPlayer.getDuration());
        musicSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
             if(fromUser){
                 musicPlayer.seekTo(progress);
             }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        playingTask = new TimerTask() {
            @Override
            public void run() {
                musicSeekBar.setProgress(musicPlayer.getCurrentPosition());
            }
        };
        timer.schedule(playingTask, 0 ,500);
    }

    public void startPause(){
        if(!haveUri)
            return;
        if(musicPlayer.isPlaying())
            pause();
        else
            start();
    }

    private void start(){
        musicPlayer.start();

    }
    private void pause(){
        musicPlayer.pause();
    }

    public void isDownloaded(boolean value){
        if(value){
            actionButton.setImageResource(R.drawable.play_icon);
            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startPause();
                }
            });
        }
    }

    public void isMeSender(boolean value){
        if(value){
            layout.setGravity(Gravity.RIGHT);
        }else {
            layout.setGravity(Gravity.LEFT);
        }
    }

    public void setSender(String senderName){
        if(senderName == null){
            sender.setVisibility(View.INVISIBLE);
            return;
        }
        sender.setText(senderName);
    }
}
