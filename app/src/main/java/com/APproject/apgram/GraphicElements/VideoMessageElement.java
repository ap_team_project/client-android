package com.APproject.apgram.GraphicElements;

import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.APproject.apgram.R;

import java.util.Timer;
import java.util.TimerTask;

public class VideoMessageElement extends RecyclerView.ViewHolder {
    LinearLayout layout;
    TextView sender;
    TextView sentTime;
    VideoView video;
    ImageButton isDownloaded;
    ImageButton startPause;

    SeekBar seekBar;

    Timer timer;
    TimerTask playingTask;


    boolean haveUri = false;


    public VideoMessageElement(@NonNull View itemView) {
        super(itemView);
        layout = itemView.findViewById(R.id.layout);
        sender = itemView.findViewById(R.id.sender);
        video = itemView.findViewById(R.id.video);
        sentTime = itemView.findViewById(R.id.messageTime);
        isDownloaded = itemView.findViewById(R.id.isDownloaded);
        startPause = itemView.findViewById(R.id.playButton);
        seekBar = itemView.findViewById(R.id.seekBar);
        seekBar.setMax(0);
    }

    public void setVideoUri(Uri uri){
        video.setVideoURI(uri);
        haveUri = true;
        seekBar.setMax(video.getDuration());
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                    video.seekTo(progress);
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
         timer= new Timer();
         playingTask = new TimerTask() {
             @Override
             public void run() {
                 seekBar.setProgress(video.getCurrentPosition());
             }
         };
        timer.schedule(playingTask,0,1000);
    }

    public void startPause(){
        if(!haveUri)
            return;
        if(video.isPlaying()){
            pause();
        }
        else {
            start();
        }
    }

    private void start(){
        video.start();

    }
    private void pause(){
        video.pause();
    }

    public void isMeSender(boolean value){
        if(value){
            layout.setGravity(Gravity.RIGHT);
        }else {
            layout.setGravity(Gravity.LEFT);
        }
    }

    public void isDownloaded(boolean value){
        if(value){
            isDownloaded.setVisibility(View.INVISIBLE);
        }
    }

    public void setSender(String senderName){
        if(senderName == null){
            sender.setVisibility(View.INVISIBLE);
            return;
        }
        sender.setText(senderName);
    }
}
