package com.APproject.apgram;

import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.APproject.apgram.GraphicElements.ChatRecyclerAdapter;
import com.APproject.apgram.client.Chats.Chat;
import com.APproject.apgram.client.Chats.ChatManager;
import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.Messages.Message;
import com.APproject.apgram.client.SimpleData;
import com.APproject.apgram.client.GraphicUpdater.UpdateManager;

public class MainActivity extends AppCompatActivity {
    public static final String tag = "main Activity";


    boolean started = false;
    ChatManager chatManager;
    RecyclerView chatsView;
    ChatRecyclerAdapter recyclerAdapter;

    TextView statue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        UpdateManager.getInstance().setMainActivityUpdater(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //add app bar
        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        //find statue edit text
        statue = findViewById(R.id.statueView);
        chatsView = findViewById(R.id.recyclerView);
        chatManager = DatabaseManager.getInstance().getChats();

        recyclerAdapter = new ChatRecyclerAdapter(this, chatManager);
        chatsView.setAdapter(recyclerAdapter);
        chatsView.setLayoutManager(new LinearLayoutManager(this));
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        DatabaseManager.getInstance().close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void setConnecting(){
        statue.setText(getResources().getString(R.string.conneting_statue));
    }
    public void setConnected(){
        statue.setText(getResources().getString(R.string.connected_statue));
    }



    public void addChat(Chat chat){
        chatManager.addChat(chat);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                recyclerAdapter.notifyDataSetChanged();
            }
        });
    }
    public void updateChat(Chat chat){
        final int index = chatManager.update(chat);

        //updating aproch?
        //re make the items from start to the chat index
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                recyclerAdapter.notifyItemRangeChanged(0, index);
            }
        });
    }
    public void updateChatIcon(String chatID){
        final int index = chatManager.find(chatID);
        if(index != -1) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    recyclerAdapter.notifyItemChanged(index);
                }
            });

        }
    }
    public void addMessage(Message message,String chatID, boolean isChatOpen){
        final int index=chatManager.addNewMessage(message,chatID,!isChatOpen);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                recyclerAdapter.notifyItemChanged(index);
            }
        });

    }
    public void setNoNewMessage(String chatID){
        final int index = chatManager.setNoNewMessage(chatID);
        if(index == -1)
            return;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                recyclerAdapter.notifyItemChanged(index);
            }
        });

    }
    public void removeChat(String chatId){
        chatManager.removeChat(chatId);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                recyclerAdapter.notifyDataSetChanged();
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Log.i(tag,"menu item selected: "+item.getTitle());
        switch (item.getItemId()){
            case R.id.settingItem:
                Intent intent1 = new Intent(this,SeeInfoActivity.class);
                intent1.setType("user");
                intent1.putExtra("id", SimpleData.getInstance().myID);
                startActivity(intent1);
                return true;
            case R.id.contactsItem:
                Intent intent2 = new Intent(this,ContactActivity.class);
                startActivity(intent2);
                return true;

            case R.id.addChatItem:
                Intent intent3 = new Intent(this, AddChatActivity.class);
                startActivity(intent3);


        }
        return super.onOptionsItemSelected(item);
    }

    public void notifyUser(){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                    r.play();
                } catch (Exception e) {
                    Log.i(tag, "cannot run the notification sound");
                }
            }
        });


    }
}
