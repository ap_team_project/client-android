package com.APproject.apgram;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.APproject.apgram.client.Chats.Access;
import com.APproject.apgram.client.Chats.Chat;
import com.APproject.apgram.client.Chats.ChatType;
import com.APproject.apgram.client.Chats.Member;
import com.APproject.apgram.client.Chats.MemberManager;
import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.Files.FileManager;
import com.APproject.apgram.client.SimpleData;
import com.APproject.apgram.client.User.User;
import com.APproject.apgram.client.Util;
import com.APproject.apgram.client.net.AddMembersToChannelRequest;
import com.APproject.apgram.client.net.AddMembersToGroupRequest;
import com.APproject.apgram.client.net.ChangeChannelIconRequest;
import com.APproject.apgram.client.net.ChangeGroupIconRequest;
import com.APproject.apgram.client.net.ChangeProfileIconRequest;
import com.APproject.apgram.client.net.RenameChannelRequest;
import com.APproject.apgram.client.net.RenameGroupRequest;
import com.APproject.apgram.client.net.UpdateChannelInformationRequest;
import com.APproject.apgram.client.net.UpdateGroupInformationRequest;
import com.APproject.apgram.client.net.UpdateUserInformationRequest;
import com.APproject.apgram.client.net.responsefromserver.RenameChannelResponse;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SeeInfoActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    String id;
    String type;

    Chat chat;

    MemberManager memberManager;

    CircleImageView icon;
    EditText name;
    TextView lastSeen;
    Button saveButton;
    Button setIconButton;

    ListView memberList;

    LinearLayout addMemberLayout;
    EditText newMemberUsername;

    ArrayList<String> newMembers = new ArrayList<>();

    String newIconPath = null;
    boolean nameChanged = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        type = intent.getType();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_info);
        //find all the views
        icon =findViewById(R.id.icon);
        name = findViewById(R.id.name);
        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                nameChanged =true;
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        lastSeen = findViewById(R.id.lastSeen);
        memberList = findViewById(R.id.memberList);
        newMemberUsername = findViewById(R.id.newMemberUsername);
        addMemberLayout = findViewById(R.id.addMemberLayout);
        id = intent.getStringExtra("id");
        setType();
    }
    private void setType(){
        if(type.equals("user")){
            haveMember(false);
            if(!id.equals(SimpleData.getInstance().myID)) {
                name.setEnabled(false);
                saveButton.setVisibility(View.INVISIBLE);
                setIconButton.setVisibility(View.INVISIBLE);
            }
            User user = null;
            user = DatabaseManager.getInstance().getUser(id);
            if(!id.equals(SimpleData.getInstance().myID))
                new UpdateUserInformationRequest(id);
            name.setText(user.getUserName());
            lastSeen.setText(user.getLastSeen());
            icon.setImageBitmap(BitmapFactory.decodeStream(FileManager.getInstance().getIcon(user.getID())));
            return;
        }
        chat = DatabaseManager.getInstance().getChat(id);
        name.setText(chat.getChatName());
        icon.setImageBitmap(BitmapFactory.decodeStream(FileManager.getInstance().getIcon(chat.getID())));

        if(type.equals("group")){
            memberManager = DatabaseManager.getInstance().getChatMembers(id);
            setAccess();
            haveMember(true);
            new UpdateGroupInformationRequest(chat.getID());
        }
        else if(type.equals("channel")){
            boolean haveMember;
            haveMember = (chat.getMyAccess()==Access.NORMAL)? false: true;
            haveMember(haveMember);
            setAccess();
            new UpdateChannelInformationRequest(chat.getID());
        }
        lastSeen.setVisibility(View.INVISIBLE);
    }
    private void haveMember(boolean value){
        if (!value) {
            LinearLayout linearLayout = findViewById(R.id.seeMembers);
            linearLayout.setVisibility(View.INVISIBLE);
            return;
        }
        //set members
        ArrayList<String> membersUsernameList;
        membersUsernameList = memberManager.getUsernames();
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.member_list_item, membersUsernameList);
        memberList.setAdapter(arrayAdapter);
        memberList.setOnItemClickListener(this);
    }

    public void setAccess(){
        if(chat.getMyAccess() == Access.NORMAL){
            name.setEnabled(false);
            addMemberLayout.setVisibility(View.INVISIBLE);
            setIconButton.setVisibility(View.INVISIBLE);
            saveButton.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, SeeInfoActivity.class);
        intent.setType("user");
        intent.putExtra("id",memberManager.get(position).getUserID());
        startActivity(intent);
    }

    public void gotToChat(View view){
        String chatID;
        if(type != "user"){
            chatID = id;
        }
        else
            chatID = Util.getTruePrivateId(id);
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("chatID",chatID);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==1 && resultCode==Activity.RESULT_OK && data!=null){
            newIconPath = data.getData().getPath();
            icon.setImageURI(data.getData());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    public void setIcon(View view){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }
    public void saveChanges(View view){
        if(nameChanged && newIconPath==null){
            return;
        }
       if(type.equals("user")){
           if(nameChanged){
               // cannot done: 7/9/20
           }
           if (newIconPath != null){
               new ChangeProfileIconRequest(SimpleData.getInstance().myID, newIconPath);
           }
       }else {
           if(nameChanged){
               if(chat.getChatType() == ChatType.GROUP){
                   new RenameGroupRequest(chat.getID(), name.getText().toString());
               }
               else if(chat.getChatType() == ChatType.CHANNEL){
                   new RenameChannelRequest(chat.getID(),name.getText().toString());
               }
           }
           if(newIconPath!=null){
               if(chat.getChatType() == ChatType.CHANNEL){
                   new ChangeChannelIconRequest(chat.getID(), newIconPath);
               }
               else if(chat.getChatType() == ChatType.GROUP){
                   new ChangeGroupIconRequest(chat.getID(), newIconPath);
               }
           }
           if(newMembers.size() != 0){
               switch (chat.getChatType()){
                   case CHANNEL:
                       new AddMembersToChannelRequest(chat.getID(),newMembers);
                       break;
                   case GROUP:
                       new AddMembersToGroupRequest(chat.getID(),newMembers);
               }
           }
       }
    }
    public void addUser(View view){
        String newMemberUserName = newMemberUsername.getText().toString();
        newMembers.add(newMemberUserName);
    }
}
