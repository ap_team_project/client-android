package com.APproject.apgram.client.Chats;

public enum Access {
    OWNER,
    ADMIN,
    NORMAL
}
