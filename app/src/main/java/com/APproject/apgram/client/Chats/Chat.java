package com.APproject.apgram.client.Chats;

import androidx.annotation.Nullable;

import com.APproject.apgram.client.Messages.Message;

import java.util.ArrayList;



public class Chat implements Comparable<Chat> {
    String id;
    ChatType type;
    String chatName;
    Message lastMessage;
    Access myAccess;
    int newMessageCount;

    public Chat(String id, ChatType type, Message lastMessage, int newMessageCount) {
        this.id = id;
        this.type = type;
        this.lastMessage = lastMessage;
        this.newMessageCount = newMessageCount;
    }

    public Chat(String id, ChatType type, String chatName, Message lastMessage, Access myAccess, int newMessageCount) {
        this.id = id;
        this.type = type;
        this.chatName = chatName;
        this.lastMessage = lastMessage;
        this.myAccess = myAccess;
        this.newMessageCount = newMessageCount;
    }

    public String getID(){
        return id;
    }
    public ChatType getChatType(){
        return type;
    }
    public String getChatName(){
        return chatName;
    }
    public int getNewMessageCount(){
        return newMessageCount;
    }

    public Message getLastMessage(){
        return lastMessage;
    }

    public Access getMyAccess(){
        return myAccess;
    }

    public void setMyAccess(Access myAccess) {
        this.myAccess = myAccess;
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    public void setLastMessage(Message lastMessage) {
        this.lastMessage = lastMessage;
    }

    public void setNewMessageCount(int newMessageCount) {
        this.newMessageCount = newMessageCount;
    }


    @Override
    public int compareTo(Chat o) {
        if(lastMessage == null || o.lastMessage == null)
            return 0;
        return lastMessage.getDate().compareTo(o.getLastMessage().getDate());
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if ((obj instanceof Chat)) {
            Chat c = (Chat) obj;
            return id.equals(c.getID());
        }
        else if(obj instanceof String){
            return id.equals(obj);
        }
        return false;
    }
}
