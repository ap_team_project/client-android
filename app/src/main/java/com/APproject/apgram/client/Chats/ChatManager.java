package com.APproject.apgram.client.Chats;

import com.APproject.apgram.client.Messages.Message;
import com.APproject.apgram.client.Messages.MessageType;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class ChatManager {
    private ArrayList<Chat> chats;
    HashMap data;
    public ChatManager(ArrayList<Chat> chats){
        this.chats = chats;
        sort();
    }

    public void sort(){
        Collections.sort(chats, Collections.<Chat>reverseOrder());
    }

    public String getLastMessageString(int index){
        String result = "";
        Message message = chats.get(index).getLastMessage();
        if(message == null)
            return "";
        String sender = message.getSender();
        if (sender != null)
            result = sender+" :";
        String content = "";
        if(message.getType() == MessageType.TEXT){
            content = message.getContent();
        }
        else if(message.getType() == MessageType.FILE){
            content = "FILE";
        }
        else if(message.getType() == MessageType.IMAGE){
            content = "IMAGE";
        }
        result.concat(content);
        return result;
    }

    public String getLastActivityTime(int index){
        Message message = chats.get(index).getLastMessage();
        if(message == null)
            return "";
        return chats.get(index).getLastMessage().getDate();
    }

    public int setNoNewMessage(String chatID){
        for(int index = 0 ; index < chats.size() ; index++){
            Chat chat = chats.get(index);
            if(chat.getID().equals(chatID)){
               chat.setNewMessageCount(0);
                return index;
            }
        }
        return -1;
    }

    public int addNewMessage(Message message,String chatID , boolean addAsNew){
        int index =chats.indexOf(chatID);
        Chat chat = chats.get(index);
        chat.setLastMessage(message);
        if(addAsNew){
            chat.setNewMessageCount(chat.getNewMessageCount()+1);
        }
        return index;
    }

    public Chat get(int index){
        return chats.get(index);
    }

    public int find(String chatID){
        for(int i = 0 ; i < chats.size() ; i++){
            if(chats.get(i).equals(chatID))
                return i;
        }
        return -1;
    }

    public int size(){
        return chats.size();
    }

    public void addChat(Chat chat){
        chats.add(chat);
        sort();
    }
    public void removeChat(String chatID){
        for(int index = 0 ; index < chats.size() ; index++){
            Chat chat = chats.get(index);
            if(chat.getID().equals(chatID)){
                chats.remove(index);
                return;
            }
        }
    }
    public int update(Chat chat){
        int index = chats.indexOf(chat);
        chats.set(index, chat);
        sort();
        return index;
    }
}
