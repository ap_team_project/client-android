package com.APproject.apgram.client.Chats;

public enum ChatType {
    GROUP,
    PRIVATE,
    CHANNEL;
}
