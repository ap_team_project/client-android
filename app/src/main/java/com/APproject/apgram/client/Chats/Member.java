package com.APproject.apgram.client.Chats;

import com.APproject.apgram.client.User.User;

public class Member {
    String userID;
    Access access;

    public Member(String userID, Access access) {
        this.userID = userID;
        this.access = access;
    }

    public String getUserID() {
        return userID;
    }

    public Access getAccess() {
        return access;
    }
}
