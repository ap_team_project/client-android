package com.APproject.apgram.client.Chats;

import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.User.User;

import java.util.ArrayList;

public class MemberManager {
    ArrayList<Member> members;

    public MemberManager(ArrayList<Member> members){
        this.members = members;
    }

    public ArrayList<String> getUsernames(){
        ArrayList<String> result = new ArrayList<>();
        for (Member member:members){
            User user = null;
            user = DatabaseManager.getInstance().getUser(member.getUserID());
            if(user == null)
                continue;
            result.add(user.getUserName());
        }
        return result;
    }

    public Member get(int index) {
        return members.get(index);
    }
}
