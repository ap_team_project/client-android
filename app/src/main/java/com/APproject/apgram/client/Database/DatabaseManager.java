package com.APproject.apgram.client.Database;

import com.APproject.apgram.client.Chats.Chat;
import com.APproject.apgram.client.Chats.ChatManager;
import com.APproject.apgram.client.Chats.Member;
import com.APproject.apgram.client.Chats.MemberManager;
import com.APproject.apgram.client.Files.FileManager;
import com.APproject.apgram.client.Messages.Message;
import com.APproject.apgram.client.Messages.MessageType;
import com.APproject.apgram.client.SimpleData;
import com.APproject.apgram.client.User.User;

import java.io.Closeable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class DatabaseManager implements Serializable, Closeable {
    static DatabaseManager singleDatabaseManager;
    public static DatabaseManager getInstance() {
        if (singleDatabaseManager==null){
            return new DatabaseManager();
        }
        return singleDatabaseManager;
    }




    HashMap<String, User> users;
    HashSet<String> contacts;
    HashMap<String , Chat> chats;
    HashMap<String , ArrayList<Message>> messages;
    HashMap<String, ArrayList<Member>> chatMembers;

    public DatabaseManager(){
        users = new HashMap<>();
        contacts = new HashSet<>();
        chats = new HashMap<>();
        messages = new HashMap<>();
        chatMembers = new HashMap<>();
        start();
    }
    public void start(){
        singleDatabaseManager = this;
    }

    public User getUser(String userID){
        if(users.containsKey(userID))
            return users.get(userID);
        return null;
    }
    public void updateUser(User user){
        users.put(user.getID(),user);
    }
    public void addUser(User user){
        users.put(user.getID(),user);
    }

    public void addChat(Chat chat){
        chat.setNewMessageCount(0);
        chat.setLastMessage(null);
        chats.put(chat.getID(),chat);
    }
    public Chat getChat(String chatID){
        if(chats.containsKey(chatID))
            return chats.get(chatID);
        return null;
    }
    public void updateChat(Chat chat){
        Chat c = chats.get(chat.getID());
        c.setChatName(chat.getChatName());
        chats.put(chat.getID(),c);
    }
    public ChatManager getChats(){
        Collection<Chat> array = chats.values();
        ArrayList<Chat> result = new ArrayList<>();
        for(Chat c : array)
            result.add(c);
        return new ChatManager(result);
    }
    public void removeChat(String chatId){
        chats.remove(chatId);
    }
    public void setChatMessagesRead(String chatId){
        if(!chats.containsKey(chatId))
            return;
        chats.get(chatId).setNewMessageCount(0);
    }
    public MemberManager getChatMembers(String chatId){
        if(chatMembers.containsKey(chatId)){
            return new MemberManager(chatMembers.get(chatId));
        }
        return new MemberManager(new ArrayList<Member>());
    }
    public void addMemberToChat(Member member, String chatId){
        ArrayList<Member> arrayList;
        if(chatMembers.containsKey(chatId)){
            arrayList = chatMembers.get(chatId);
        }
        else {
            arrayList = new ArrayList<>();
        }
        chatMembers.put(chatId, arrayList);
    }
    public void setChatMembers(String chatID, ArrayList<Member> members){
        chatMembers.put(chatID,members);
        for(Member member: members){
            if(member.getUserID().equals(SimpleData.getInstance().myID)){
                Chat chat = chats.get(chatID);
                if (chat == null)
                    return;
                chat.setMyAccess(member.getAccess());
                chats.put(chatID,chat);
                return;
            }
        }
    }
    public void addContact(String userId){
        contacts.add(userId);
    }
    public ArrayList<User> getContacts(){
        ArrayList<User> result = new ArrayList<>();
        for(String userID:contacts){
            User user = getUser(userID);
            if(user != null)
                result.add(user);
        }
        return result;
    }
    public ArrayList<Message> getChatMassages(String chatId){
        if(messages.containsKey(chatId)){
            return messages.get(chatId);
        }
        return new ArrayList<>();
    }
    public void addMessage(Message message, String chatId){
        ArrayList<Message> arrayList = getChatMassages(chatId);
        arrayList.add(message);
        messages.put(chatId,arrayList);
        if(chatId.charAt(0) == 'p')
            return;
        Chat chat = chats.get(chatId);
        chat.setNewMessageCount(chat.getNewMessageCount());
        chat.setLastMessage(message);
        chats.put(chatId,chat);
    }
    public void setDownloaded(String chatID, String messageId){
        if(!messages.containsKey(chatID))
            return;
        ArrayList<Message> arrayList = getChatMassages(chatID);
        for(Message message:arrayList){
            if(message.getId().equals(messageId)){
                if(message.getType() != MessageType.TEXT)
                    message.setDownloaded();
                return;
            }
        }
    }

    public void setNoNewMessage(String chatID){
        if(chats.containsKey(chatID)){
            Chat chat = chats.get(chatID);
            chat.setNewMessageCount(0);
            chats.put(chatID,chat);
        }
    }

    @Override
    public void close(){
        FileManager.getInstance().saveDatabaseManager();
    }
}
