package com.APproject.apgram.client.Files;

import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DownloadFile implements Serializable {
    String id;
    int index = 1;
    int count;
    long eachStepSize;
    FileType type;
    Lock lock;

    public DownloadFile(int count, long eachStepSize, String id ,FileType type) {
        this.count = count;
        this.eachStepSize = eachStepSize;
        this.type = type;
        this.id = id;
        lock = new ReentrantLock();
    }

    public boolean save(int index , String hexString){
        lock.lock();
        long start = (index-1)*eachStepSize;
        if(type == FileType.ICON){
            FileManager.getInstance().saveIcon(id, hexString,start);
        }
        else{
            FileManager.getInstance().saveFileMessage(id,hexString,start);
        }
        this.index++;
        if(this.index +1 == count) {
            lock.unlock();
            return true;
        }
        lock.unlock();
        return false;
    }
}
enum FileType{
    MESSAGE_FILE,
    ICON
}
