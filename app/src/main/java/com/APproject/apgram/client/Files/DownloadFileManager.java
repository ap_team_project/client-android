package com.APproject.apgram.client.Files;

import java.io.Serializable;
import java.util.HashMap;


/**
 * can save to file for saving the state
 */
public class DownloadFileManager implements Serializable {
    private static DownloadFileManager singleDownloadFileManager;

    public static DownloadFileManager getInstance(){
        if(singleDownloadFileManager == null)
            return new DownloadFileManager();
        return singleDownloadFileManager;
    }

    public DownloadFileManager(){
        downloads = new HashMap<>();
        singleDownloadFileManager = this;
    }

    HashMap<String,DownloadFile> downloads;

    public boolean saveIcon(String id, int index, long eachStepCount, int count, String hexString){
        DownloadFile downloadFile;
        if(downloads.containsKey(id)){
            downloadFile = new DownloadFile(count, eachStepCount, id, FileType.ICON);
            downloads.put(id, downloadFile);
        }else {
            downloadFile = downloads.get(id);
        }
        return downloadFile.save(index, hexString);
    }
    public boolean saveFileMessage(String id, int index, long eachStepCount, int count, String hexString){
        DownloadFile downloadFile;
        if(downloads.containsKey(id)){
            downloadFile = new DownloadFile(count, eachStepCount, id, FileType.MESSAGE_FILE);
            downloads.put(id, downloadFile);
        }else {
            downloadFile = downloads.get(id);
        }
        return downloadFile.save(index, hexString);
    }

}
