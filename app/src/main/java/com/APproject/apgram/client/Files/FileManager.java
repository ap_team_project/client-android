package com.APproject.apgram.client.Files;

import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;

public class FileManager {
    private static FileManager singleton_fileManager;
    public static FileManager getInstance(){
        return singleton_fileManager;
    }

    private static final String messageFilesDir = "messageFiles";
    private static final String iconFilesDir = "icons";


    public File filesDir;

    public FileManager(File filesDir){
        this.filesDir = filesDir;
        singleton_fileManager = this;
        File file = new File(filesDir,messageFilesDir);
        if(!file.exists())
            file.mkdir();
        file = new File(filesDir,iconFilesDir);
        if(!file.exists())
            file.mkdir();
        loadDatabaseManager();
    }

    public void loadDatabaseManager(){
        File file = new File(filesDir,"databaseManager");
        if(!file.exists()) {
            new DatabaseManager();
            return;
        }
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            ((DatabaseManager)in.readObject()).start();
        } catch (IOException | ClassNotFoundException e) {}

    }
    public void saveDatabaseManager(){
        DatabaseManager db = DatabaseManager.getInstance();
        File file = new File(filesDir,"databaseManager");
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {}
        }
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            out.writeObject(db);
            out.flush();
            out.close();
        } catch (IOException e) {}
    }

    public void saveFileMessage(String messageID, String hexString,long start){
        File file = new File(new File(filesDir,messageFilesDir),messageID);
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {}
        }
        try {
            RandomAccessFile out = new RandomAccessFile(file, "rw");
            byte[] bytes = Util.decodeHexString(hexString);
            out.seek(start);
            out.write(bytes);
            out.close();
        } catch (Exception e) {}
    }
    public void saveIcon(String id, String hexString, long start){
        File file = new File(new File(filesDir,iconFilesDir),id);
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {}
        }
        try {
            RandomAccessFile out = new RandomAccessFile(file, "rw");
            byte[] bytes = Util.decodeHexString(hexString);
            out.seek(start);
            out.write(bytes);
            out.close();
        } catch (Exception e) {}
    }

    public void setFile(String localID, String id){
        File file = new File(new File(filesDir, messageFilesDir),localID);
        if(!file.exists()){
            return;
        }
        file.renameTo(new File(file.getParent(), id));
    }

    public InputStream getIcon(String id){
        File file = new File(new File(filesDir, iconFilesDir),id);
        if(!file.exists()){
            return null;
        }
        try {
            InputStream in = new FileInputStream(file);
            return in;
        } catch (FileNotFoundException e) {
            return null;
        }
    }
    public InputStream getMessageFileStream(String messageID){
        File file = new File(new File(filesDir, messageFilesDir),messageID);
        try {
            InputStream in = new FileInputStream(file);
            return in;
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    public File getMessageFile(String messageID){
        File file = new File(new File(filesDir,messageFilesDir),messageID);
        if(!file.exists())
            return null;
        return file;
    }
}
