package com.APproject.apgram.client.Files;

import com.APproject.apgram.client.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class UploadFile {
    public static final int eachCountSize = 10000;

    String filePath;
    InputStream in;
    int index = 1;
    long size;
    int count;

    public UploadFile(String filePath) throws FileNotFoundException {
        this.filePath = filePath;
        File f = new File(filePath);
        in = new FileInputStream(f);
        size = f.length();
        count = (int) Math.ceil((size+0.0)/eachCountSize);
    }

    public int getCount() {
        return count;
    }

    public String next(){
        index++;
        if(index > count)
            return null;
        int thisStepSize = (index != count)? eachCountSize: (int) (size - index * eachCountSize);
        byte[] byteStr = new byte[ thisStepSize];
        try {
            in.read(byteStr);
            return Util.encodeHexString(byteStr);
        } catch (IOException e) {}
        return null;
    }

}
