package com.APproject.apgram.client.GraphicUpdater;

import com.APproject.apgram.ChatActivity;
import com.APproject.apgram.client.Messages.Message;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ChatActivityUpdater {
    String id;
    ChatActivity chatActivity;
    Lock lock;

    public ChatActivityUpdater(ChatActivity chatActivity) {
        this.chatActivity = chatActivity;
        id = chatActivity.getChatID();
        lock = new ReentrantLock();
    }

    public String getChatID() {
        return id;
    }

    public void updateChatIcon(String chatId){
        if(id.equals(chatId))
            chatActivity.recreate();
    }
    public void updateUserState(String userID){
        if(id.charAt(0) == 'p'){
            if(id.contains(userID)){
                chatActivity.recreate();
            }
        }
    }
    public void chatRemove(String chatID){
        if(id.equals(chatID)){
            chatActivity.finish();
        }
    }
    public void setDownloaded(String messageID){
        lock.lock();
        chatActivity.setDownloaded(messageID);
        lock.unlock();
    }
    public void addMessage(Message message){
        lock.lock();
        chatActivity.addMessage(message);
        lock.unlock();
    }
}
