package com.APproject.apgram.client.GraphicUpdater;

import com.APproject.apgram.MainActivity;
import com.APproject.apgram.client.Chats.Chat;
import com.APproject.apgram.client.Messages.Message;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MainActivityUpdater {
    MainActivity mainActivity;
    Lock lock;

    public MainActivityUpdater(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        lock = new ReentrantLock();
    }

    public void setConnected(){
        mainActivity.setConnected();
    }
    public void setConnecting(){
        mainActivity.setConnecting();
    }
    public void setNoNewMessage(String chatID){
        mainActivity.setNoNewMessage(chatID);
    }
    public void updateChat(Chat chat){
        lock.lock();
        mainActivity.updateChat(chat);
        lock.unlock();
    }
    public void updateChatIcon(String chatID){
        lock.lock();
        mainActivity.updateChatIcon(chatID);
        lock.unlock();
    }
    public void removeChat(String chatID){
        lock.lock();
        mainActivity.removeChat(chatID);
        lock.unlock();
    }
    public void addChat(Chat chat){
        lock.lock();
        mainActivity.addChat(chat);
        lock.unlock();
    }
    public void addMessage(Message message, String chatID, boolean isChatOpen) {
        if(!isChatOpen){
            lock.lock();
            mainActivity.addMessage(message,chatID,isChatOpen);
            lock.unlock();
        }
        mainActivity.addMessage(message,chatID,isChatOpen);
    }
    public void notifyUser(){
        mainActivity.notifyUser();
    }

}
