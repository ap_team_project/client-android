package com.APproject.apgram.client.GraphicUpdater;

import com.APproject.apgram.ChatActivity;
import com.APproject.apgram.MainActivity;
import com.APproject.apgram.client.Chats.Chat;
import com.APproject.apgram.client.Messages.Message;
import com.APproject.apgram.client.NetworkStatue;

public class UpdateManager {
    private static UpdateManager single_updateManager;

    public static UpdateManager getInstance() {
        if(single_updateManager == null)
            return new UpdateManager();
        return single_updateManager;
    }

    MainActivityUpdater mainActivityUpdater = null;
    ChatActivityUpdater chatActivityUpdater = null;

    private UpdateManager(){}

    public void setMainActivityUpdater(MainActivity mainActivity) {
        this.mainActivityUpdater = new MainActivityUpdater(mainActivity);
    }

    public void setChatActivityUpdater(ChatActivity chatActivity) {
        this.chatActivityUpdater = new ChatActivityUpdater(chatActivity);
        mainActivityUpdater.setNoNewMessage(chatActivityUpdater.getChatID());
    }
    public void removeChatActivity(){
        chatActivityUpdater = null;
    }


    public void setNetworkStatue(NetworkStatue statue){
        return;
//        if(mainActivityUpdater == null)
//            return;
//        if(statue == NetworkStatue.CONNECTED){
//            mainActivityUpdater.setConnected();
//        }else
//            mainActivityUpdater.setConnecting();
    }
    public void updateChat(Chat chat){
        mainActivityUpdater.updateChat(chat);
    }
    public void chatIconUpdated(String chatID){
//        mainActivityUpdater.updateChatIcon(chatID);
//        if(chatActivityUpdater != null){
//            chatActivityUpdater.updateChatIcon(chatID);
//        }
    }
    public void updateUserState(String userID){
//        if (chatActivityUpdater == null)
//            return;
//        chatActivityUpdater.updateUserState(userID);
    }
    public void removeChat(String chatID){
//        mainActivityUpdater.removeChat(chatID);
//        if(chatActivityUpdater == null){
//            return;
//        }
//        chatActivityUpdater.chatRemove(chatID);
    }
    public void addChat(Chat chat){
//        mainActivityUpdater.addChat(chat);
    }
    public void haveMessage(String chatID, Message message){
//        boolean isChatOpen = false;
//        if(chatActivityUpdater != null)
//            isChatOpen = chatActivityUpdater.getChatID().equals(chatID);
//        mainActivityUpdater.addMessage(message, chatID, isChatOpen);
//        mainActivityUpdater.notifyUser();
//        if(isChatOpen){
//            chatActivityUpdater.addMessage(message);
//        }
    }
    public void setDownloaded(String messageID){
//        if(chatActivityUpdater != null){
//            chatActivityUpdater.setDownloaded(messageID);
//        }
    }
}
