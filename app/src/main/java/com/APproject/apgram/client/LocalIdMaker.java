package com.APproject.apgram.client;

import java.io.Serializable;
import java.util.HashMap;

public class LocalIdMaker implements Serializable {
    private static LocalIdMaker singleLocalIdMaker;
    public static LocalIdMaker getInstance(){
        if(singleLocalIdMaker == null)
            return new LocalIdMaker();
        return singleLocalIdMaker;
    }

    HashMap<String ,Integer> data;

    public LocalIdMaker(){
        data = new HashMap<>();
        saveSingleInstance();
    }
    public void saveSingleInstance(){
        singleLocalIdMaker = this;
    }

    public String getLocalId(String baseId){
        String result;
        if (data.containsKey(baseId)) {
            int n = data.get(baseId);
            result = baseId + "##"+n;
            data.put(baseId, n+1);
        }
        else {
            data.put(baseId, 1);
            result = baseId +"##0";
        }
        return result;
    }
}
