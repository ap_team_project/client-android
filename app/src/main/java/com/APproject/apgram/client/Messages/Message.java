package com.APproject.apgram.client.Messages;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Message {
    String id = null;
    String textContent = null;
    String senderId = null;
    MessageType type;
    Date sendDate;
    boolean isDownloaded;

    public Message(String id, String textContent, String senderId, MessageType type, Date sendDate) {
        this.id = id;
        this.textContent = textContent;
        this.senderId = senderId;
        this.type = type;
        this.sendDate = sendDate;
    }

    public Message(String id, String senderId, MessageType type, Date sendDate, boolean isDownloaded) {
        this.id = id;
        this.senderId = senderId;
        this.type = type;
        this.sendDate = sendDate;
        this.isDownloaded = isDownloaded;
    }

    public String getContent(){
       return textContent;
   }
    public String getId() {
        return id;
    }
    public String getSender(){
        return senderId;
    }
    public MessageType getType(){
        return type;
    }
    public String getDate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm");
        return dateFormat.format(sendDate);
    }
    public boolean isDownloaded(){
       return isDownloaded;
    }

    public void setDownloaded(){
        isDownloaded = true;
    }
}

