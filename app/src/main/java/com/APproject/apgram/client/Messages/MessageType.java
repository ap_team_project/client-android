package com.APproject.apgram.client.Messages;

public enum MessageType {
    TEXT,
    FILE,
    IMAGE,
    VIDEO,
    MUSIC
}
