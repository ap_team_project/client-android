package com.APproject.apgram.client;

import com.APproject.apgram.client.User.User;

public class SimpleData {
    public static SimpleData singleSimpleData;

    public static SimpleData getInstance(){
        return singleSimpleData;
    }


    public User MyUser;
    public final String serverIp;
    public final int serverPort;
    public String myUserName;
    public String myID;
    public String password;


    public SimpleData(String serverIp, int serverPort, String myUserName, String password) {
        this.serverIp = serverIp;
        this.serverPort = serverPort;
        this.myUserName = myUserName;
        this.password = password;
        singleSimpleData = this;
    }
}
