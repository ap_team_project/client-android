package com.APproject.apgram.client.User;

import java.util.Date;

public class User {
    String userName;
    String ID;
    boolean isOnline;
    Date lastSeen;

    public User(String userName, String ID, boolean isOnline, Date lastSeen) {
        this.userName = userName;
        this.ID = ID;
        this.isOnline = isOnline;
        this.lastSeen = lastSeen;
    }

    public String getLastSeen(){
        if(isOnline){
            return "online";
        }
        return lastSeen.toString();
    }

    public String getID(){
        return ID;
    }

    public String getUserName(){
        return userName;
    }

}
