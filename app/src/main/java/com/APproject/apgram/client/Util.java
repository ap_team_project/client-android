package com.APproject.apgram.client;

public class Util {
    //String decoder
    public static byte[] decodeHexString(String hexString) {
        if (hexString.length() % 2 == 1) {
            throw new IllegalArgumentException(
                    "Invalid hexadecimal String supplied.");
        }

        byte[] bytes = new byte[hexString.length() / 2];
        for (int i = 0; i < hexString.length(); i += 2) {
            bytes[i / 2] = hexToByte(hexString.substring(i, i + 2));
        }
        return bytes;
    }
    private static byte hexToByte(String hexString) {
        int firstDigit = toDigit(hexString.charAt(0));
        int secondDigit = toDigit(hexString.charAt(1));
        return (byte) ((firstDigit << 4) + secondDigit);
    }
    private static int toDigit(char hexChar) {
        int digit = Character.digit(hexChar, 16);
        if(digit == -1) {
            throw new IllegalArgumentException(
                    "Invalid Hexadecimal Character: "+ hexChar);
        }
        return digit;
    }
    //byte encoder
    public static String encodeHexString(byte[] byteArray) {
        StringBuffer hexStringBuffer = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            hexStringBuffer.append(byteToHex(byteArray[i]));
        }
        return hexStringBuffer.toString();
    }
    private static String byteToHex(byte num) {
        char[] hexDigits = new char[2];
        hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
        hexDigits[1] = Character.forDigit((num & 0xF), 16);
        return new String(hexDigits);
    }


    public static String getTruePrivateId(String userID){
        String myID = SimpleData.getInstance().myID;
        int myInt = Integer.parseInt(myID.substring(1));
        int userInt = Integer.parseInt(userID.substring(1));
        if(myInt > userInt){
            return "p"+userID+"#"+myID;
        }
        else {
            return "p"+myID+"#"+userID;
        }
    }

    public static String getUserIdFromPrivateChat(String chatID){
        if(chatID.charAt(0) != 'p')
            return chatID;

        String userID = chatID.substring(1).replace(SimpleData.getInstance().myID,"");
        userID = userID.replace("#","");
        return userID;
    }

}
