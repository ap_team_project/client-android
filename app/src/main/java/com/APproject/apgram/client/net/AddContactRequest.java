package com.APproject.apgram.client.net;

import com.google.gson.Gson;

public class AddContactRequest extends Requests implements Runnable {
    private String requestTYPE;
    private String USERNAME;

    public AddContactRequest(String USERNAME) {
        this.requestTYPE = "AddContactRequest";
        this.USERNAME = USERNAME;
        new Thread(this).start();
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        boolean flag = true;
        while (flag){
            if (Connection.getInstance().getisConnected() == false) {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                flag = false;
            }
        }
        Connection.getInstance().sendGSONFILE(GSONFILE);
    }
}
