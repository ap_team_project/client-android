package com.APproject.apgram.client.net;

import com.google.gson.Gson;

import java.util.ArrayList;

public class AddMembersToChannelRequest extends Requests implements Runnable {
    private String requestTYPE;
    private String channelID;
    private ArrayList<String> membersID;

    public AddMembersToChannelRequest(String channelID, ArrayList<String> membersID) {
        this.membersID = new ArrayList<>();
        this.requestTYPE = "AddMembersToChannelRequest";
        this.channelID = channelID;
        this.membersID = membersID;
        new Thread(this).start();
    }

    public String getChannelID() {
        return channelID;
    }

    public ArrayList getMembersID() {
        return membersID;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        boolean flag = true;
        while (flag){
            if (Connection.getInstance().getisConnected() == false) {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                flag = false;
            }
        }
        Connection.getInstance().sendGSONFILE(GSONFILE);
    }
}