package com.APproject.apgram.client.net;

import com.google.gson.Gson;

import java.util.ArrayList;

public class AddMembersToGroupRequest extends Requests implements Runnable {
    private String requestTYPE;
    private String groupID;
    private ArrayList<String> membersUSERNAME;

    public AddMembersToGroupRequest(String groupID, ArrayList<String> membersID) {
        this.membersUSERNAME = new ArrayList<>();
        this.requestTYPE = "AddMembersToGroupRequest";
        this.groupID = groupID;
        this.membersUSERNAME = membersID;
        new Thread(this).start();
    }

    public String getGroupID() {
        return groupID;
    }

    public ArrayList<String> getMembersUSERNAME() {
        return membersUSERNAME;
    }


    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        boolean flag = true;
        while (flag){
            if (Connection.getInstance().getisConnected() == false) {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                flag = false;
            }
        }
        Connection.getInstance().sendGSONFILE(GSONFILE);
    }
}