package com.APproject.apgram.client.net;


import com.APproject.apgram.client.Files.DownloadFileManager;
import com.APproject.apgram.client.Files.UploadFile;
import com.APproject.apgram.client.LocalIdMaker;
import com.google.gson.Gson;
import java.io.FileNotFoundException;

public class ChangeProfileIconRequest extends Requests implements Runnable {
    private String requestTYPE;
    private String userID;
    private int index;
    public static final long eachStepCount = 10000;
    private int count;
    private  String hexString;
    private String fileAddress;

    public ChangeProfileIconRequest(String userID , String fileAddresst) {
        this.requestTYPE = "ChangeProfileIconRequest";
        this.userID = userID;
        this.fileAddress = fileAddresst;
        new Thread(this).start();
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public String getUserID() {
        return userID;
    }

    @Override
    public void run() {
        boolean flag = true;
        while (flag) {
            if (Connection.getInstance().getisConnected() == false) {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    System.out.println(e);
                }
            } else {
                flag = false;
            }
        }
        UploadFile uploadFile = null;
        try {
            uploadFile = new UploadFile(fileAddress);
            this.count = uploadFile.getCount();
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
        for (int index = 1; index <= this.count; index++) {
            this.index = index;
            this.hexString = uploadFile.next();
            DownloadFileManager.getInstance().saveIcon(userID,index,eachStepCount,count,hexString);
            String GSONFILE = new Gson().toJson(this);
            Connection.getInstance().sendGSONFILE(GSONFILE);
        }
    }
}
