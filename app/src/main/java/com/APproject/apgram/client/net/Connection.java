package com.APproject.apgram.client.net;

import android.util.Log;

import com.APproject.apgram.LoginActivity;
import com.APproject.apgram.client.GraphicUpdater.UpdateManager;
import com.APproject.apgram.client.NetworkStatue;
import com.APproject.apgram.client.SimpleData;
import com.APproject.apgram.client.net.responsefromserver.responses;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import sbu.ap.jsonParser.json;

public class Connection implements Runnable{
    public static String tag = "connection447";
    private static Connection singleConnection;
    public static Connection getInstance(){
        return singleConnection;
    }

    Socket socket;
    InputStream in;
    OutputStream out;
    boolean isConnected = false;

    boolean isFirstSignIn;
    boolean createAccount;
    LoginActivity loginActivity;

    public Connection(boolean isFirstSignIn, boolean createAccount, LoginActivity loginActivity) {
        this.isFirstSignIn = isFirstSignIn;
        this.createAccount = createAccount;
        this.loginActivity = loginActivity;
        singleConnection = this;
        new Thread(this).start();
    }

    @Override
    public void run() {
        String serverIP = SimpleData.getInstance().serverIp;
        int port = SimpleData.getInstance().serverPort;
        String userName = SimpleData.getInstance().myUserName;
        String password =SimpleData.getInstance().password;
        while (true){
            try {
                socket = new Socket(serverIP,port);
                Log.i(tag,"connected to server");
                break;
            }catch (UnknownHostException e) {
                Log.i(tag,e.toString());
            } catch (IOException e) {
                Log.i(tag,e.toString());
            }
        }
        try {
            in = socket.getInputStream();
            out = socket.getOutputStream();
        } catch (IOException e) {}
        UpdateManager.getInstance().setNetworkStatue(NetworkStatue.CONNECTING);
        Log.i(tag,"sending first request");
        json jsonData = new json();
        jsonData.put("username",userName);
        jsonData.put("password",password);
        jsonData.put("creatingAccount",createAccount);
        Log.i(tag,"first request: "+jsonData.toString());
        sendGSONFILE(jsonData.toString());
        String str = receiveGSONFILE();
        Log.i(tag,"fist response: "+str);
        json respond = json.parser(str);
        Log.i(tag, "first received respond: "+respond.toString());
        if(isFirstSignIn){
            if (createAccount){
                String id = null;
                try {
                    boolean created = (boolean)respond.getDataUsingDot("created?");
                    if(created){
                        id = (String) respond.getDataUsingDot("userID");
                    }
                } catch (Exception e) {}
                if(id == null){
                    loginActivity.signInResult(false,null);
                    return;
                }
                loginActivity.signInResult(true,id);
            }
            else {
                //removed because of time
                return;
            }
        }
        else {
            try {
                if(!(boolean)respond.getDataUsingDot("signedIN"))
                    return;
            } catch (Exception e) {}
        }
        isConnected = true;
        UpdateManager.getInstance().setNetworkStatue(NetworkStatue.CONNECTED);
        try {
            while (true) {
                while (in.read() != 1) ;
                byte[] byteLength = new byte[4];
                in.read(byteLength);
                int length = ByteBuffer.wrap(byteLength).getInt();
                byte[] byteStr = new byte[length];
                in.read(byteStr);
                String string = new String(byteStr);
                Log.i(tag,"recieve: "+string);
                new responses(string);
            }
        }catch (Exception e){}
        isConnected = false;
        UpdateManager.getInstance().setNetworkStatue(NetworkStatue.CONNECTING);
    }

    public void sendGSONFILE (String gsonfile){
        Log.i(tag,"sent: "+gsonfile);
        ByteBuffer buffer = ByteBuffer.allocate(4+gsonfile.length());
        buffer.putInt(gsonfile.length());
        buffer.put(gsonfile.getBytes());
        try {
            out.write(buffer.array());
            out.flush();
        } catch (IOException e) {}
        Log.i(tag,"sent: "+gsonfile);
    }
    public String receiveGSONFILE (){
        try {
            while (in.read() != 1);
            byte[] byteLength = new byte[4];
            in.read(byteLength);
            int length = ByteBuffer.wrap(byteLength).getInt();
            byte[] byteStr = new byte[length];
            in.read(byteStr);
            return new String(byteStr);
        }catch (Exception e){}
        return null;
    }

    public boolean getisConnected() {
        return isConnected;
    }
}
