package com.APproject.apgram.client.net;

import com.APproject.apgram.client.SimpleData;
import com.google.gson.Gson;

public class CreateAccountRequest extends Requests{
    private String requestTYPE;
    private String USERNAME;
    private String PASSWORD;

    public CreateAccountRequest() {
        this.requestTYPE = "CreateAccountRequest";
        this.USERNAME = SimpleData.getInstance().myUserName;
        this.PASSWORD = SimpleData.getInstance().myUserName;
    }

    public String parsGSON() {
        String GSONFILE = new Gson().toJson(this);
        return GSONFILE;
    }
}