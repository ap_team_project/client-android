package com.APproject.apgram.client.net;

import com.google.gson.Gson;
import java.util.ArrayList;

public class CreateChannelRequest extends Requests implements Runnable {
    private String requestTYPE;
    private String channelName;
    private String adminID;
    private ArrayList<String> membersUSERNAME;

    public CreateChannelRequest(String adminID, String channelName, ArrayList<String> membersUSERNAME) {
        this.membersUSERNAME = new ArrayList<>();
        this.requestTYPE = "CreateChannelRequest";
        this.channelName = channelName;
        this.adminID = adminID;
        this.membersUSERNAME = membersUSERNAME;
        new Thread(this).start();
    }
    public String getAdminID() {
        return adminID;
    }

    public String getChannelName() {
        return channelName;
    }

    public ArrayList<String> getMembersUSERNAME(){
        return membersUSERNAME;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        boolean flag = true;
        while (flag){
            if (Connection.getInstance().getisConnected() == false) {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                flag = false;
            }
        }
        Connection.getInstance().sendGSONFILE(GSONFILE);
    }
}