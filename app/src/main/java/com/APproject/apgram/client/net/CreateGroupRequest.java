package com.APproject.apgram.client.net;

import com.google.gson.Gson;
import java.util.ArrayList;

public class CreateGroupRequest extends Requests implements Runnable {
    private String requestTYPE;
    private String groupNAME;
    private String adminID;
    private ArrayList<String> membersUSERNAME;

    public CreateGroupRequest(String adminID, String groupNAME, ArrayList<String> membersUSERNAME) {
        this.membersUSERNAME = new ArrayList<>();
        this.requestTYPE = "CreateGroupRequest";
        this.groupNAME = groupNAME;
        this.adminID = adminID;
        this.membersUSERNAME = membersUSERNAME;
        new Thread(this).start();
    }
    public String getAdminID() {
        return adminID;
    }

    public String getGroupNAME() {
        return groupNAME;
    }

    public ArrayList<String> getMembersUSERNAME(){
        return membersUSERNAME;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        boolean flag = true;
        while (flag){
            if (Connection.getInstance().getisConnected() == false) {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                flag = false;
            }
        }
        Connection.getInstance().sendGSONFILE(GSONFILE);
    }
}