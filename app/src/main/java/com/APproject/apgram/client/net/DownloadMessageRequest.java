package com.APproject.apgram.client.net;

import com.google.gson.Gson;

public class DownloadMessageRequest extends Requests implements Runnable {
    private String requestTYPE;
    private String messageID;
    private String chatID;

    public DownloadMessageRequest(String messageID , String chatID) {
        this.requestTYPE = "DownloadMessageRequest";
        this.messageID = messageID;
        this.chatID = chatID;
        new Thread(this).start();
    }

    public String getMessageID() {
        return messageID;
    }

    public String getChatID() {
        return chatID;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        boolean flag = true;
        while (flag){
            if (Connection.getInstance().isConnected == false){
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                flag = false;
            }
        }
        Connection.getInstance().sendGSONFILE(GSONFILE);
    }
}
