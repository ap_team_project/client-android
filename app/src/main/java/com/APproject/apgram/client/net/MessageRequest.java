package com.APproject.apgram.client.net;

import com.google.gson.Gson;

public class MessageRequest extends Requests implements Runnable{
    private String requestTYPE;
    private String chatID;
    private String text;

    public MessageRequest(String text, String chatID) {
        this.requestTYPE = "MessageRequest";
        this.chatID = chatID;
        this.text = text;
        new Thread(this).start();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getChatID() {
        return chatID;
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        boolean flag = true;
        while (flag){
            if (Connection.getInstance().getisConnected() == false) {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                flag = false;
            }
        }
        Connection.getInstance().sendGSONFILE(GSONFILE);
    }
}