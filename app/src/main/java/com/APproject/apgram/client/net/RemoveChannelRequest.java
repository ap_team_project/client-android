package com.APproject.apgram.client.net;

import com.google.gson.Gson;

public class RemoveChannelRequest extends Requests implements Runnable {
    private String requestTYPE;
    private String channelID;

    public RemoveChannelRequest(String groupID) {
        this.requestTYPE = "RemoveChannelRequest";
        this.channelID = groupID;
        new Thread(this).start();
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public String getChannelID() {
        return channelID;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        boolean flag = true;
        while (flag){
            if (Connection.getInstance().getisConnected() == false) {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                flag = false;
            }

        }
        Connection.getInstance().sendGSONFILE(GSONFILE);
    }
}
