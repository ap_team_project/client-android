package com.APproject.apgram.client.net;

import com.google.gson.Gson;

public class RenameChannelRequest extends Requests implements Runnable {
    private String requestTYPE;
    private String channelID;
    private String channelNewNAME;

    public RenameChannelRequest(String channelID, String channelNewNAME) {
        this.requestTYPE = "RenameChannelRequest";
        this.channelID = channelID;
        this.channelNewNAME = channelNewNAME;
        new Thread(this).start();
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public String getChannelID() {
        return channelID;
    }

    public String getChannelNewNAME() {
        return channelNewNAME;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        boolean flag = true;
        while (flag){
            if (Connection.getInstance().getisConnected() == false) {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                flag = false;
            }
        }
        Connection.getInstance().sendGSONFILE(GSONFILE);
    }
}
