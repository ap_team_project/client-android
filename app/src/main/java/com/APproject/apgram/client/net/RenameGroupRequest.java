package com.APproject.apgram.client.net;

import com.google.gson.Gson;

public class RenameGroupRequest extends Requests implements Runnable{
    private String requestTYPE;
    private String groupID;
    private String groupNewNAME;

    public RenameGroupRequest(String groupID, String groupNewNAME) {
        this.requestTYPE = "RenameGroupRequest";
        this.groupID = groupID;
        this.groupNewNAME = groupNewNAME;
        new Thread(this).start();
    }

    public String getRequestTYPE() {
        return requestTYPE;
    }

    public String getGroupID() {
        return groupID;
    }

    public String getGroupNewNAME() {
        return groupNewNAME;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        boolean flag = true;
        while (flag){
            if (Connection.getInstance().getisConnected() == false) {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                flag = false;
            }
        }
        Connection.getInstance().sendGSONFILE(GSONFILE);
    }
}
