package com.APproject.apgram.client.net;

import com.APproject.apgram.client.Files.DownloadFile;
import com.APproject.apgram.client.Files.DownloadFileManager;
import com.APproject.apgram.client.Files.UploadFile;
import com.APproject.apgram.client.LocalIdMaker;
import com.google.gson.Gson;


import java.io.FileNotFoundException;

public class SendFileRequest extends Requests implements Runnable {
    private String requestTYPE;
    private String chatID;
    private String localID;
    private String fileTYPE;
    private int index;
    public static final long eachStepCount = 10000;
    private int count;
    private  String hexString;
    private String fileAddress;

    public SendFileRequest(String chatID , String fileAddress , String fileTYPE) {
        this.requestTYPE = "SendFileRequest";
        this.chatID = chatID;
        this.localID = LocalIdMaker.getInstance().getLocalId("F");
        this.fileTYPE = fileTYPE;
        this.fileAddress = fileAddress;
        new Thread(this).start();
    }

    public String getChatID() {
        return getChatID();
    }

    public String getLocalID(){
        return getLocalID();
    }

    public int getIndex() {
        return index;
    }

    public long getEachStepCount() {
        return eachStepCount;
    }

    public int getCount() {
        return count;
    }

    @Override
    public void run() {
        boolean flag = true;
        while (flag){
            if (Connection.getInstance().getisConnected() == false){
                try {
                    Thread.sleep(20000);
                }
                catch (InterruptedException e) {
                    System.out.println(e);
                }
            }
            else {
                flag = false;
            }
        }
        UploadFile uploadFile = null;
        try {
            uploadFile = new UploadFile(fileAddress);
            this.count = uploadFile.getCount();
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
        for (int index = 1 ; index <= this.count ; index++){
            this.index = index;
            this.hexString = uploadFile.next();
            DownloadFileManager.getInstance().saveFileMessage(localID , index , eachStepCount , count , hexString);
            String GSONFILE = new Gson().toJson(this);
            Connection.getInstance().sendGSONFILE(GSONFILE);
        }
    }
}