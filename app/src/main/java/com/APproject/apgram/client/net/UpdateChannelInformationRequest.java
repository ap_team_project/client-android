package com.APproject.apgram.client.net;

import com.google.gson.Gson;

public class UpdateChannelInformationRequest extends Requests implements Runnable{
    private String requestTYPE;
    private String channelID;

    public UpdateChannelInformationRequest(String channelID) {
        this.requestTYPE = "UpdateChannelInformationRequest";
        this.channelID = channelID;
        new Thread(this).start();
    }

    public String getChannelID() {
        return channelID;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        boolean flag = true;
        while (flag){
            if (Connection.getInstance().getisConnected() == false) {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                flag = false;
            }
        }
        Connection.getInstance().sendGSONFILE(GSONFILE);
    }
}
