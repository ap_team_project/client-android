package com.APproject.apgram.client.net;

import com.google.gson.Gson;

public class UpdateGroupInformationRequest extends Requests implements Runnable{
    private String requestTYPE;
    private String groupID;

    public UpdateGroupInformationRequest(String groupID) {
        this.requestTYPE = "UpdateGroupInformationRequest";
        this.groupID = groupID;
        new Thread(this).start();
    }

    public String getGroupID() {
        return groupID;
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        boolean flag = true;
        while (flag){
            if (Connection.getInstance().getisConnected() == false) {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                flag = false;
            }
        }
        Connection.getInstance().sendGSONFILE(GSONFILE);
    }
}
