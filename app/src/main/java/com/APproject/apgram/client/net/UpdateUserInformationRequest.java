package com.APproject.apgram.client.net;

import com.APproject.apgram.client.User.User;
import com.google.gson.Gson;
import java.util.Date;

public class UpdateUserInformationRequest extends Requests implements Runnable {
    private String requestTYPE;
    private String userID;

    public UpdateUserInformationRequest(String userID) {
        this.requestTYPE = "UpdateUserInformationRequest";
        this.userID = userID;
        new Thread(this).start();
    }

    @Override
    public void run() {
        String GSONFILE = new Gson().toJson(this);
        boolean flag = true;
        while (flag){
            if (Connection.getInstance().getisConnected() == false) {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                flag = false;
            }
        }
        Connection.getInstance().sendGSONFILE(GSONFILE);
    }
}
