package com.APproject.apgram.client.net.responsefromserver;

import com.APproject.apgram.client.Chats.Access;
import com.APproject.apgram.client.Chats.Chat;
import com.APproject.apgram.client.Chats.ChatType;
import com.APproject.apgram.client.Chats.Member;
import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.GraphicUpdater.UpdateManager;

import java.util.ArrayList;

public class AddChannelResponse implements Runnable{
    private String serverRequestTYPE;
    private String chatID;
    private String chatNAME;
    private ArrayList<String> membersUSERNAME;
    private ArrayList<String> access;
    private String destinationID;

    public AddChannelResponse() {
        this.membersUSERNAME = new ArrayList<>();
        this.access = new ArrayList<>();
    }

    public void process(){
        new Thread(this).start();
    }

    public ArrayList<String> getMembersUSERNAME() {
        return membersUSERNAME;
    }

    public String getChatID() {
        return chatID;
    }

    public String getserverRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getDestinationID() {
        return destinationID;
    }

    public String getChatNAME() {
        return chatNAME;
    }

    public ArrayList<String> getAccess() {
        return access;
    }

    @Override
    public void run() {

        ArrayList<Member> memberArrayList = new ArrayList<>();
        for (int i = 0 ; i < getMembersUSERNAME().size() ; i++){
            if(getAccess().get(i) == "NORMAL"){
                memberArrayList.add(i,new Member(getMembersUSERNAME().get(i), Access.NORMAL));
            }
            else {
                memberArrayList.add(i,new Member(getMembersUSERNAME().get(i), Access.OWNER));
            }
        }
        DatabaseManager.getInstance().setChatMembers(getChatID(),memberArrayList);
        Chat chat = new Chat(getChatID(), ChatType.CHANNEL,getChatNAME(),null,Access.NORMAL,0);
        DatabaseManager.getInstance().addChat(chat);
        UpdateManager.getInstance().addChat(chat);
    }
}
