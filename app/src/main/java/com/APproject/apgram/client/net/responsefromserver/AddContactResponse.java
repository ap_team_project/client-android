package com.APproject.apgram.client.net.responsefromserver;

import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.User.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddContactResponse implements Runnable {
    private String serverRequestTYPE;
    private String userName;
    private String userID;
    private String lastSeen;
    private String destinationID;

    public AddContactResponse() {
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserID() {
        return userID;
    }

    public String getDestinationID() {
        return destinationID;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void process (){
        new Thread(this).start();
    }

    @Override
    public void run() {
        try {
            Date date = new SimpleDateFormat("dd/MM/yyyy").parse(getLastSeen());
            User user = new User(getUserName(),getUserID(),false , date);
            DatabaseManager.getInstance().addUser(user);
            DatabaseManager.getInstance().addContact(getUserID());
        } catch (ParseException e) {
            System.out.println(e);
        }
    }
}
