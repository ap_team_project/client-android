package com.APproject.apgram.client.net.responsefromserver;

import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.Files.DownloadFile;
import com.APproject.apgram.client.Files.DownloadFileManager;
import com.APproject.apgram.client.Files.UploadFile;
import com.APproject.apgram.client.GraphicUpdater.UpdateManager;
import com.APproject.apgram.client.net.Connection;
import com.APproject.apgram.client.net.SendFileRequest;
import com.google.gson.Gson;

import java.io.FileNotFoundException;

public class ChangeChannelIconResponse implements Runnable{
    private String serverRequestTYPE;
    private String chatID;
    private int index;
    public static final long eachStepCount = 10000;
    private int count;
    private  String hexString;
    private String fileAddress;
    private String destinationID;
    

    public ChangeChannelIconResponse() {
    }

    public void process(){
        new Thread(this).start();
    }

    public String getserverRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getChatID() {
        return chatID;
    }

    public String getDestinationID() {
        return destinationID;
    }

    public int getIndex() {
        return index;
    }

    public static long getEachStepCount() {
        return eachStepCount;
    }

    public int getCount() {
        return count;
    }

    public String getHexString() {
        return hexString;
    }

    public String getFileAddress() {
        return fileAddress;
    }


    @Override
    public void run() {
        DownloadFileManager.getInstance().saveIcon(getChatID() , getIndex() , getEachStepCount() , getCount() , getHexString());
        if (getIndex() == getCount()){
            //todo debug here
            DatabaseManager.getInstance().setDownloaded(getChatID(),null);
            UpdateManager.getInstance().setDownloaded(null);
            UpdateManager.getInstance().chatIconUpdated(getChatID());
        }
    }
}
