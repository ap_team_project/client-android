package com.APproject.apgram.client.net.responsefromserver;

import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.Files.DownloadFileManager;
import com.APproject.apgram.client.GraphicUpdater.UpdateManager;
import com.APproject.apgram.client.net.SendFileRequest;

public class ChangeProfileIconResponse implements Runnable{
    private String serverRequestTYPE;
    private String userID;
    private int index;
    public static final long eachStepCount = 10000;
    private int count;
    private  String hexString;
    private String fileAddress;
    private String destinationID;

    public ChangeProfileIconResponse() {
    }

    public void process(){
        new Thread(this).start();
    }

    public String getUserID() {
        return userID;
    }

    public int getIndex() {
        return index;
    }

    public static long getEachStepCount() {
        return eachStepCount;
    }

    public int getCount() {
        return count;
    }

    public String getHexString() {
        return hexString;
    }

    public String getFileAddress() {
        return fileAddress;
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        DownloadFileManager.getInstance().saveIcon(getUserID() , getIndex() , getEachStepCount() , getCount() , getHexString());
        if (getIndex() == getCount()){
            //todo debug here
            DatabaseManager.getInstance().setDownloaded(getUserID(),null);
            UpdateManager.getInstance().setDownloaded(null);
            UpdateManager.getInstance().chatIconUpdated(getUserID());
        }
    }
}
