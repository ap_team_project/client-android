package com.APproject.apgram.client.net.responsefromserver;

public class CreateAccountResponse implements Runnable{
    private String serverRequestTYPE;
    private String USERNAME;
    private String PASSWORD;
    // todo do we need chatID ??

    public CreateAccountResponse() {
    }

    public void process(){
        new Thread(this).start();
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    @Override
    public void run() {

    }
}
