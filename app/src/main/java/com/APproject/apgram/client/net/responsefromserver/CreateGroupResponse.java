package com.APproject.apgram.client.net.responsefromserver;

import com.APproject.apgram.client.net.SendFileRequest;

public class CreateGroupResponse implements Runnable{
    private String serverRequestTYPE;
    private String groupNAME = null;
    private String adminID = null;

    public CreateGroupResponse() {
    }

    public void process(){
        new Thread(this).start();
    }

    public String getGroupNAME() {
        return groupNAME;
    }

    public String getAdminID() {
        return adminID;
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    @Override
    public void run() {

    }
}
