package com.APproject.apgram.client.net.responsefromserver;

import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.Files.DownloadFileManager;
import com.APproject.apgram.client.GraphicUpdater.UpdateManager;
import com.APproject.apgram.client.Messages.Message;
import com.APproject.apgram.client.Messages.MessageType;

import java.util.Date;

public class DownloadMessageResponse implements Runnable{
    private String serverRequestTYPE;
    private String messageID;
    private String chatID;
    private int index;
    public static final long eachStepCount = 10000;
    private int count;
    private  String hexString;
    private String fileAddress;
    private String destinationID;

    public DownloadMessageResponse() {
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    public int getIndex() {
        return index;
    }

    public static long getEachStepCount() {
        return eachStepCount;
    }

    public int getCount() {
        return count;
    }
    public String getHexString() {
        return hexString;
    }
    public String getFileAddress() {
        return fileAddress;
    }
    public String getDestinationID() {
        return destinationID;
    }

    public String getMessageID() {
        return messageID;
    }
    public String getChatID() {
        return chatID;
    }
    public void process (){
        new Thread(this).start();
    }
    @Override
    public void run() {
        DownloadFileManager.getInstance().saveFileMessage(getChatID(),getIndex(),getEachStepCount(),getCount(),getHexString());
        if (getCount() == getIndex()) {
            DatabaseManager.getInstance().setDownloaded(getChatID(), getMessageID());
        }
    }
}