package com.APproject.apgram.client.net.responsefromserver;

import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.GraphicUpdater.UpdateManager;
import com.APproject.apgram.client.Messages.Message;
import com.APproject.apgram.client.Messages.MessageType;

import java.util.Date;

public class MessageResponse implements Runnable{
    private String serverRequestTYPE;
    private String chatID;
    private String messageID;
    private String text;
    private String destinationID;

    public MessageResponse() {
    }

    public void process(){
        new Thread(this).start();
    }

    public String getChatID() {
        return chatID;
    }

    public String getText() {
        return text;
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getMessageID() {
        return messageID;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        Message message = new Message(getMessageID(),getText(),null, MessageType.TEXT,new Date());
        DatabaseManager.getInstance().addMessage(message,getChatID());
        UpdateManager.getInstance().updateChat(DatabaseManager.getInstance().getChat(getChatID()));
        UpdateManager.getInstance().haveMessage(getChatID(),message);
    }
}
