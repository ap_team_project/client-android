package com.APproject.apgram.client.net.responsefromserver;

public class RemoveChannelResponse implements Runnable {
    private String serverRequestTYPE;
    private String channelID;

    public RemoveChannelResponse() {
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getChannelID() {
        return channelID;
    }

    public void process(){
        new Thread(this).start();
    }

    @Override
    public void run() {

    }
}
