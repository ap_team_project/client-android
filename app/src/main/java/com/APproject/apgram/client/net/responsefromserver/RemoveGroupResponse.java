package com.APproject.apgram.client.net.responsefromserver;

public class RemoveGroupResponse implements Runnable {
    private String serverRequestTYPE;
    private String groupID;

    public RemoveGroupResponse() {
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getGroupID() {
        return groupID;
    }
    public void process(){
        new Thread(this).start();
    }

    @Override
    public void run() {

    }
}
