package com.APproject.apgram.client.net.responsefromserver;

public class RenameChannelResponse implements Runnable{
    private String serverRequestTYPE;
    private String channelID;
    private String channelNewNAME;

    public RenameChannelResponse() {
    }

    public void process(){
        new Thread(this).start();
    }

    public String getChannelID() {
        return channelID;
    }

    public String getChannelNewNAME() {
        return channelNewNAME;
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    @Override
    public void run() {

    }
}
