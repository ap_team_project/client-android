package com.APproject.apgram.client.net.responsefromserver;

public class RenameGroupResponse implements Runnable{
    private String serverRequestTYPE;
    private String groupID;
    private String groupNewNAME;

    public RenameGroupResponse() {
    }

    public void process(){
        new Thread(this).start();
    }

    public String getGroupID() {
        return groupID;
    }

    public String getGroupNewNAME() {
        return groupNewNAME;
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    @Override
    public void run() {

    }
}
