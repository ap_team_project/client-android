package com.APproject.apgram.client.net.responsefromserver;

import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.Files.DownloadFile;
import com.APproject.apgram.client.Files.DownloadFileManager;
import com.APproject.apgram.client.Files.UploadFile;
import com.APproject.apgram.client.GraphicUpdater.UpdateManager;
import com.APproject.apgram.client.LocalIdMaker;
import com.APproject.apgram.client.Messages.Message;
import com.APproject.apgram.client.Messages.MessageType;
import com.APproject.apgram.client.net.Connection;
import com.APproject.apgram.client.net.Requests;
import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.util.Date;

public class SendFileResponse implements Runnable{
    private String serverRequestTYPE;
    private String chatID;
    private String messageID;
    private String fileTYPE;
    private int index;
    public static final long eachStepCount = 10000;
    private int count;
    private  String hexString;
    private String fileAddress;
    private String destinationID;


    public void process(){
            new Thread(this).start();
        }

    public String getChatID() {
            return getChatID();
        }

    public String getLocalID(){
            return getLocalID();
        }

    public int getIndex() {
            return index;
        }

    public long getEachStepCount() {
            return eachStepCount;
        }

    public int getCount() {
            return count;
        }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getMessageID() {
        return messageID;
    }

    public String getHexString() {
        return hexString;
    }

    public String getFileAddress() {
        return fileAddress;
    }

    public String getFileTYPE() {
        return fileTYPE;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
        public void run() {
        DownloadFileManager.getInstance().saveFileMessage(getChatID(),getIndex(),getEachStepCount(),getCount(),getHexString());
        if (getCount() == getIndex()){
            if (getFileTYPE() == "VIDEO") {
                Message message = new Message(getMessageID(),getDestinationID(), MessageType.VIDEO,new Date(),true);
                DatabaseManager.getInstance().setDownloaded(getChatID(),getMessageID());
                UpdateManager.getInstance().setDownloaded(getMessageID());
                DatabaseManager.getInstance().addMessage(message,getChatID());
                UpdateManager.getInstance().haveMessage(getChatID(),message);
            }
            else if (getFileTYPE() == "MUSIC"){
                Message message = new Message(getMessageID(),getDestinationID(), MessageType.MUSIC,new Date(),true);
                DatabaseManager.getInstance().setDownloaded(getChatID(),getMessageID());
                UpdateManager.getInstance().setDownloaded(getMessageID());
                DatabaseManager.getInstance().addMessage(message,getChatID());
                UpdateManager.getInstance().haveMessage(getChatID(),message);
            }
            else if (getFileTYPE() == "IMAGE"){
                Message message = new Message(getMessageID(),getDestinationID(), MessageType.IMAGE,new Date(),true);
                DatabaseManager.getInstance().setDownloaded(getChatID(),getMessageID());
                UpdateManager.getInstance().setDownloaded(getMessageID());
                DatabaseManager.getInstance().addMessage(message,getChatID());
                UpdateManager.getInstance().haveMessage(getChatID(),message);
            }
            else {
                Message message = new Message(getMessageID(),getDestinationID(), MessageType.FILE,new Date(),true);
                DatabaseManager.getInstance().setDownloaded(getChatID(),getMessageID());
                UpdateManager.getInstance().setDownloaded(getMessageID());
                DatabaseManager.getInstance().addMessage(message,getChatID());
                UpdateManager.getInstance().haveMessage(getChatID(),message);
            }
        }
        }
}

