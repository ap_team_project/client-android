package com.APproject.apgram.client.net.responsefromserver;

public class UpdateChannelInformationResponse implements Runnable{
    private String serverRequestTYPE;
    private String channelID;

    public UpdateChannelInformationResponse() {
    }

    public void process(){
        new Thread(this).start();
    }

    public String getChannelID() {
        return channelID;
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    @Override
    public void run() {

    }
}
