package com.APproject.apgram.client.net.responsefromserver;

import com.APproject.apgram.client.Chats.Access;
import com.APproject.apgram.client.Chats.Chat;
import com.APproject.apgram.client.Chats.ChatType;
import com.APproject.apgram.client.Chats.Member;
import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.GraphicUpdater.UpdateManager;

import java.util.ArrayList;

public class UpdateChatResponse implements Runnable{
    private String serverRequestTYPE;
    private String chatID;
    private String chatNAME;
    private String chatTYPE;
    private ArrayList<String> membersUSERNAME;
    private ArrayList<String> access;
    private String destinationID;

    public UpdateChatResponse() {
        this.membersUSERNAME = new ArrayList<>();
    }

    public void process(){
        new Thread(this).start();
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getChatID() {
        return chatID;
    }

    public String getChatNAME() {
        return chatNAME;
    }

    public ArrayList<String> getMembersUSERNAME() {
        return membersUSERNAME;
    }

    public String getChatTYPE() {
        return chatTYPE;
    }

    public String getDestinationID() {
        return destinationID;
    }

    public ArrayList<String> getAccess() {
        return access;
    }

    @Override
    public void run() {
        ArrayList<Member> memberArrayList = new ArrayList<>();
        for (int i = 0 ; i < getMembersUSERNAME().size() ; i++){
            if (getAccess().get(i) == "NORMAL"){
                memberArrayList.add(i,new Member(getMembersUSERNAME().get(i), Access.NORMAL));
            }else{
                memberArrayList.add(i,new Member(getMembersUSERNAME().get(i), Access.OWNER));
            }
        }
        DatabaseManager.getInstance().setChatMembers(getChatID(),memberArrayList);
        if (getChatTYPE() == "PRIVATE"){
            Chat chat = new Chat(getChatID(), ChatType.PRIVATE,null,0);
            UpdateManager.getInstance().updateChat(chat);
        }
        else if (getChatTYPE() == "GROUP"){
            Chat chat = new Chat(getChatID(),ChatType.GROUP,getChatNAME(),null,Access.NORMAL,0);
            UpdateManager.getInstance().updateChat(chat);
        }
        else {
            Chat chat = new Chat(getChatID(),ChatType.CHANNEL,getChatNAME(),null,Access.NORMAL,0);
            UpdateManager.getInstance().updateChat(chat);
        }
    }
}
