package com.APproject.apgram.client.net.responsefromserver;

public class UpdateGroupInformationResponse implements Runnable{
    private String serverRequestTYPE;
    private String groupID;

    public UpdateGroupInformationResponse() {
    }

    public void process(){
        new Thread(this).start();
    }

    public String getGroupID() {
        return groupID;
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    @Override
    public void run() {

    }
}
