package com.APproject.apgram.client.net.responsefromserver;

import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.GraphicUpdater.UpdateManager;
import com.APproject.apgram.client.User.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UpdateUserInformationResponse implements Runnable{
    private String serverRequestTYPE;
    private String userName;
    private String userID;
    private String lastSeen;
    private String destinationID;

    public UpdateUserInformationResponse() {
    }

    public void process(){
        new Thread(this).start();
    }

    public String getUserName() {
        return userName;
    }

    public String getUserID() {
        return userID;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public String getServerRequestTYPE() {
        return serverRequestTYPE;
    }

    public String getDestinationID() {
        return destinationID;
    }

    @Override
    public void run() {
        try {
            Date  date = new SimpleDateFormat("dd/MM/yyyy").parse(getLastSeen());
            User user = new User(getUserName(),getUserID(),true,date);
            DatabaseManager.getInstance().updateUser(user);
            UpdateManager.getInstance().updateUserState(getUserID());
        } catch (ParseException e) {
            System.out.println(e);
        }
    }
}
