package com.APproject.apgram.client.net.responsefromserver;

import com.google.gson.Gson;

public class responses implements Runnable {
    private String GSONFILE;
    private String serverResponseTYPE;

    private UpdateChatResponse createChannelResponse;
    private CreateGroupResponse createGroupResponse;
    private AddGroupResponse addChannelResponse;
    private AddChannelResponse addGroupResponse;
    private CreateAccountResponse createAccountResponse;
    private MessageResponse messageResponse;
    private RenameGroupResponse renameGroupResponse;
    private RenameChannelResponse renameChannelResponse;
    private UpdateChannelInformationResponse updateChannelInformationResponse;
    private UpdateGroupInformationResponse updateGroupInformationResponse;
    private UpdateUserInformationResponse updateUserInformationResponse;
    private SendFileResponse sendFileResponse;
    private ChangeChannelIconResponse changeChannelIconResponse;
    private ChangeGroupIconResponse changeGroupIconServerResponse;
    private ChangeProfileIconResponse changeProfileIconServerResponse;
    private DownloadMessageResponse downloadMessageResponse;
    private UpdateChatResponse updateChatResponse;
    private AddContactResponse addContactResponse;

    public responses(String GSONFILE) {
        this.GSONFILE = GSONFILE;
        new Thread(this).start();
    }

    @Override
    public void run() {
        int beg_index = 22;
        int end_index = GSONFILE.indexOf(",") - 1;
        serverResponseTYPE = GSONFILE.substring(beg_index,end_index);
        if (serverResponseTYPE.equals("CreateChannelServerRequest")){
           this.createChannelResponse = new Gson().fromJson(GSONFILE, UpdateChatResponse.class);
           this.createChannelResponse.process();
        }
        else if (serverResponseTYPE.equals("CreateGroupServerRequest")){
            this.createGroupResponse = new Gson().fromJson(GSONFILE, CreateGroupResponse.class );
            this.createGroupResponse.process();
        }
        else if (serverResponseTYPE.equals("AddChannelServerRequest")){
            this.addChannelResponse = new Gson().fromJson(GSONFILE, AddGroupResponse.class);
            this.addChannelResponse.process();
        }
        else if (serverResponseTYPE.equals("AddGroupServerRequest")){
            this.addGroupResponse = new Gson().fromJson(GSONFILE, AddChannelResponse.class);
            this.addGroupResponse.process();
        }
        else if (serverResponseTYPE.equals("CreateAccountServerRequest")){
            this.createAccountResponse = new Gson().fromJson(GSONFILE, CreateAccountResponse.class);
            this.createAccountResponse.process();
        }
        else if (serverResponseTYPE.equals("SendMessageToClient")){
            this.messageResponse = new Gson().fromJson(GSONFILE,MessageResponse.class );
            this.messageResponse.process();
        }
        else if (serverResponseTYPE.equals("RenameGroupServerRequest")){
            this.renameGroupResponse = new Gson().fromJson(GSONFILE,RenameGroupResponse.class );
            this.renameGroupResponse.process();
        }
        else if (serverResponseTYPE.equals("RenameChannelServerRequest")){
            this.renameChannelResponse = new Gson().fromJson(GSONFILE,RenameChannelResponse.class );
            this.renameChannelResponse.process();
        }
        else if (serverResponseTYPE.equals("UpdateChannelInformationServerRequest")){
            this.updateChannelInformationResponse = new Gson().fromJson(GSONFILE, UpdateChannelInformationResponse.class);
            this.updateChannelInformationResponse.process();
        }
        else if (serverResponseTYPE.equals("UpdateGroupInformationServerRequest")){
            this.updateGroupInformationResponse = new Gson().fromJson(GSONFILE, UpdateGroupInformationResponse.class);
            this.updateGroupInformationResponse.process();
        }
        else if (serverResponseTYPE.equals("UpdateUserInformationServerRequest")){
            this.updateUserInformationResponse = new Gson().fromJson(GSONFILE, UpdateUserInformationResponse.class);
            this.updateUserInformationResponse.process();
        }
        else if (serverResponseTYPE.equals("SendFileToClient")){
            this.sendFileResponse = new Gson().fromJson(GSONFILE, SendFileResponse.class);
            this.sendFileResponse.process();
        }
        else if (serverResponseTYPE.equals("ChangeChannelIconServerRequest")){
            this.changeChannelIconResponse = new Gson().fromJson(GSONFILE, ChangeChannelIconResponse.class);
            this.changeChannelIconResponse.process();
        }
        else if (serverResponseTYPE.equals("ChangeGroupIconServerRequest")){
            this.changeGroupIconServerResponse = new Gson().fromJson(GSONFILE, ChangeGroupIconResponse.class);
            this.changeGroupIconServerResponse.process();
        }
        else if (serverResponseTYPE.equals("DownloadMessageServerRequest")){
            this.downloadMessageResponse = new Gson().fromJson(GSONFILE , DownloadMessageResponse.class);
            this.downloadMessageResponse.process();
        }
        else if (serverResponseTYPE.equals("UpdateChatServerRequest")){
            this.updateChatResponse = new Gson().fromJson(GSONFILE , UpdateChatResponse.class);
            this.updateChatResponse.process();
        }
        else if (serverResponseTYPE.equals("AddContactServerRequest")) {
            this.addContactResponse = new Gson().fromJson(GSONFILE,AddContactResponse.class);
            this.addContactResponse.process();
        }
        else if (serverResponseTYPE.equals("ChangeProfileIconServerRequest")){
            this.changeProfileIconServerResponse = new Gson().fromJson(GSONFILE, ChangeProfileIconResponse.class);
            this.changeProfileIconServerResponse.process();
        }
    }
}
