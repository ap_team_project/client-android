package com.APproject.apgram.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.APproject.apgram.database.dao.ChatDao;
import com.APproject.apgram.database.dao.ChatUserDao;
import com.APproject.apgram.database.dao.MessageDao;
import com.APproject.apgram.database.dao.UserDao;
import com.APproject.apgram.database.model.Chat;
import com.APproject.apgram.database.model.ChatUser;
import com.APproject.apgram.database.model.Message;
import com.APproject.apgram.database.model.User;
import com.APproject.apgram.database.utils.Convertors;

@Database(entities = {User.class, Chat.class, ChatUser.class, Message.class}, version = 1)
@TypeConverters(Convertors.class)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
    public abstract ChatDao chatDao();
    public abstract ChatUserDao chatUserDao();
    public abstract MessageDao messageDao();
}
