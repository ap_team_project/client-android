package com.APproject.apgram.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.APproject.apgram.database.model.Chat;

import java.util.List;

@Dao
public interface ChatDao {
    @Query("SELECT * FROM chat")
    List<Chat> getAll();

    @Query("SELECT * FROM chat WHERE id IN (:chatIds)") // todo change query to be sorted by lastMessageDate
    List<Chat> loadAllByIds(int[] chatIds);

    @Query("SELECT * FROM chat WHERE chat_name LIKE :chatName LIMIT 1")
    Chat findByName(String chatName);

    @Insert
    void insertAll(Chat... chats);

    @Update
    void update(Chat... chat);

    @Delete
    void delete(Chat chat);
}
