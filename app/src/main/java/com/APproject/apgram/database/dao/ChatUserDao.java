package com.APproject.apgram.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.APproject.apgram.database.model.ChatUser;


import java.util.List;

@Dao
public interface ChatUserDao {
    @Query("SELECT * FROM chat_user")
    List<ChatUser> getAll();

    @Query("SELECT * FROM chat_user WHERE chatId IN (:chatIds)")
    List<ChatUser> loadAllByChatIds(int[] chatIds);

    @Query("SELECT * FROM chat_user WHERE chatId LIKE :chatId LIMIT 1")
    ChatUser findByChatId(int chatId);

    @Insert
    void insertAll(ChatUser... chatUser);

    @Update
    void update(ChatUser... chatUser);

    @Delete
    void delete(ChatUser chatUser);
}
