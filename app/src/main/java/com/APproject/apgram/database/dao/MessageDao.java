package com.APproject.apgram.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.APproject.apgram.database.model.Message;

import java.util.List;
import java.util.UUID;

@Dao
public interface MessageDao {
    @Query("SELECT * FROM message")
    List<Message> getAll();

    @Query("SELECT * FROM MESSAGE WHERE uuid LIKE :uuid LIMIT 1")
    Message getByUUID(UUID uuid);

    // todo write query to get last message of a chatId (should sort by date)
//    Message getLastMessageByChatId(int chatId);

    @Query("SELECT * FROM message WHERE chatId LIKE :chatIds")
    List<Message> loadAllByChatId(int chatIds);


    @Insert
    void insertAll(Message... message);

    @Update
    void update(Message... message);

    @Delete
    void delete(Message message);
}
