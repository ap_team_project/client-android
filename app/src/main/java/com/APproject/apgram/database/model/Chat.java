package com.APproject.apgram.database.model;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


import java.util.Date;
import java.util.UUID;

@Entity(tableName = "chat")
public class Chat {
    @PrimaryKey
    @NonNull
    public UUID id;

    @ColumnInfo(name = "chat_name")
    public String chatName; // if chat is of type USER the name set to the firstName of user

    @ColumnInfo(name = "last_message")
    public Date lastMessage;

    public ChatType chatType;

    public boolean hasPic;

    public boolean isPicDownloaded;

    @Ignore
    public Bitmap pic;
}

