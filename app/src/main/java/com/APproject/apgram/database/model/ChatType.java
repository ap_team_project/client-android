package com.APproject.apgram.database.model;

public enum ChatType {
    USER,
    GROUP,
    CHANNEL,
}
