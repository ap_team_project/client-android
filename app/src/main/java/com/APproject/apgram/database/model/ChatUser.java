package com.APproject.apgram.database.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.util.UUID;

@Entity(tableName = "chat_user", foreignKeys = {
        @ForeignKey(entity = User.class, parentColumns = {"id"}, childColumns = {"userId"}),
        @ForeignKey(entity = Chat.class, parentColumns = {"id"}, childColumns = {"chatId"})
})
public class ChatUser {

    @PrimaryKey(autoGenerate = true)
    public int id;

    public UUID userId;

    public UUID chatId;

    public RoleType role;
}
