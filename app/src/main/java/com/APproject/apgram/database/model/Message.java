package com.APproject.apgram.database.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import java.util.Date;
import java.util.UUID;

@Entity(tableName = "message", primaryKeys = {"uuid", "chatId"}, foreignKeys = {
        @ForeignKey(entity = Chat.class, parentColumns = {"id"}, childColumns = {"chatId"}),
        @ForeignKey(entity = User.class, parentColumns = {"id"}, childColumns = {"senderId"})
})
public class Message {
    @NonNull
    public UUID uuid;

    @NonNull
    public UUID chatId;

    public MessageType messageType;

    public boolean isMeSender;

    public UUID senderId;  // when the isMeSender is not True

    public boolean isDownloaded;    // when messageType is not TEXT

    public String text;

    public Date sendDate;
}
