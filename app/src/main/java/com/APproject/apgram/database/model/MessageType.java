package com.APproject.apgram.database.model;

public enum MessageType {
    TEXT,
    VOICE,
    IMAGE,
    VIDEO,
    FILE,
}
