package com.APproject.apgram.database.model;

public enum RoleType {
    OWNER,
    ADMIN,
    MEMBER
}
