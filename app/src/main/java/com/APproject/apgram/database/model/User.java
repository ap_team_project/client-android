package com.APproject.apgram.database.model;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;
import java.util.UUID;

@Entity(tableName = "user")
public class User {
    @PrimaryKey
    @NonNull
    public UUID id;

    @ColumnInfo(name = "user_name")
    public String userName;

    @ColumnInfo(name = "first_name")
    public String firstName;

    public boolean isContact;

    @ColumnInfo(name = "contact_name")
    public String contactName;  // name of the contact that client has chosen for this user

    @ColumnInfo(name = "update_uuid")
    public String updateUUID;

    @ColumnInfo(name = "has_profile_pic")
    public boolean hasPic;

    @ColumnInfo(name = "last_seen")
    public Date lastSeen;

    @Ignore
    public Bitmap pic;

}
