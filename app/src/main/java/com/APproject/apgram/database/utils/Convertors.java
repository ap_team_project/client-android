package com.APproject.apgram.database.utils;

import androidx.room.TypeConverter;

import com.APproject.apgram.client.Chats.ChatType;
import com.APproject.apgram.database.model.MessageType;
import com.APproject.apgram.database.model.RoleType;

import java.util.Date;
import java.util.UUID;

public class Convertors {
    @TypeConverter
    public static String fromChatType(ChatType chatType) {
        return chatType.name();
    }

    @TypeConverter
    public static ChatType toChatType(String chatType) {
        return ChatType.valueOf(chatType);
    }

    @TypeConverter
    public static String fromMessageType(MessageType messageType) {
        return messageType.name();
    }

    @TypeConverter
    public static MessageType toMessageType(String messageType) {
        return MessageType.valueOf(messageType);
    }

    @TypeConverter
    public static String fromRoleType(RoleType roleType){
        return roleType.name();
    }

    @TypeConverter
    public static RoleType toRoleType(String roleType){
        return RoleType.valueOf(roleType);
    }

    @TypeConverter
    public static long fromDate(Date date) {
        return date == null ? null : date.getTime();
    }

    @TypeConverter
    public static Date toDate(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static String fromUUID(UUID uuid) {
        return uuid.toString();
    }

    @TypeConverter
    public static UUID toUUID(String string) {
        return UUID.fromString(string);
    }
}
