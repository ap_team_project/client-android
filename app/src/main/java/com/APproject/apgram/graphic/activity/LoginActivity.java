package com.APproject.apgram.graphic.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.APproject.apgram.MainActivity;
import com.APproject.apgram.R;
import com.APproject.apgram.client.Database.DatabaseManager;
import com.APproject.apgram.client.Files.FileManager;
import com.APproject.apgram.client.SimpleData;
import com.APproject.apgram.client.User.User;
import com.APproject.apgram.client.net.ChangeProfileIconRequest;
import com.APproject.apgram.client.net.Connection;
import com.APproject.apgram.database.AppDatabase;
import com.APproject.apgram.graphic.controller.LoginActivityController;
import com.APproject.apgram.network.AppSocket;
import com.APproject.apgram.network.Request;
import com.APproject.apgram.network.RequestManager;
import com.APproject.apgram.network.output.LoginRequest;
import com.APproject.apgram.tools.StringTools;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class LoginActivity extends AppCompatActivity {
    final String tag = "LOGIN_ACTIVITY"; //for debug
    final int PICK_IMAGE_REQUEST = 1;

    Uri newUserIconUri;

    EditText usernameEditText;
    EditText passwordEditText;
    CheckBox createAccountCheckBox;
    EditText serverIpEditText;
    Button signInButton;

    boolean isCreatingAccount;

    Button setIconButton;
    CircleImageView userIconImageView;
    ImageView clearIconButton;

    RelativeLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(loadSharedPreferencesData()){
            // todo load other sections of neede class (File, AppSocker, ...)
            goToMainActivity();
        }
        createDatabaseIfNeeded();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        LoginActivityController.getInstance().setLoginActivity(this);
        findItems();
        setViewCheckersAndListeners();
    }

    // used in onCreate
    private void findItems(){
        layout = findViewById(R.id.loginActivityLayout);
        usernameEditText = findViewById(R.id.userName);
        passwordEditText = findViewById(R.id.password);
        createAccountCheckBox = findViewById(R.id.createAccountCheck);
        serverIpEditText = findViewById(R.id.serverIP);
        setIconButton = findViewById(R.id.setIconButton);
        userIconImageView = findViewById(R.id.userIcon);
        clearIconButton = findViewById(R.id.clearIcon);
        signInButton = findViewById(R.id.signIn);
    }

    // used in onCreate
    private void setViewCheckersAndListeners(){
        usernameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!StringTools.checkUsername(s.toString())){
                    usernameEditText.setError(getResources().getString(R.string.userNameError));
                }
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!StringTools.checkUsername(s.toString())){
                    passwordEditText.setError(getResources().getString(R.string.passwordError));
                }
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        serverIpEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!StringTools.checkIP(s.toString())){
                    serverIpEditText.setError(getResources().getString(R.string.ipError));
                }
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        createAccountCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setCreateAccountState(isChecked);
                isCreatingAccount = isChecked;
            }
        });
    }

    private boolean loadSharedPreferencesData(){
        SharedPreferences sharedPreferences = getSharedPreferences("server_data", MODE_PRIVATE);
        if(sharedPreferences.getBoolean("logged_in", false)){
            String serverIP = sharedPreferences.getString("server_ip", null);
            int serverPort = sharedPreferences.getInt("server_port", 0);

            // get use name and password from another shared pref
//            String username = sharedPreferences.getString("username", null);
//            String password = sharedPreferences.getString("password", null);
//            String userUUIDStr = sharedPreferences.getString("user_uuid", null);
            // todo create the shared data class
            return true;
        }
       return false;
    }

    private void createDatabaseIfNeeded(){
        SharedPreferences sharedPreferences = getSharedPreferences("database", MODE_PRIVATE);
        String databaseName = sharedPreferences.getString("database_name", null);
        if(databaseName == null){
            databaseName = "apgram_db";
            AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, databaseName).build();
            SharedPreferences.Editor shEditor = sharedPreferences.edit();
            shEditor.putString("database_name", databaseName);
            shEditor.commit();

        }
    }

    private void setCreateAccountState(boolean isChecked){
        if(!isChecked){
            setIconButton.setVisibility(View.INVISIBLE);
            clearIconButton.setVisibility(View.INVISIBLE);
            clearSelectedIcon();
            Log.i(tag, "Set to login to account");
        }
        else {
            setIconButton.setVisibility(View.VISIBLE);
            Snackbar.make(layout, R.string.createAccountSnackbarMessage, Snackbar.LENGTH_LONG).show();
            Log.i(tag, "Set to create account");
        }
    }
    public void clearSelectedIcon(View view){clearSelectedIcon();}
    private void clearSelectedIcon(){
        newUserIconUri = null;
        userIconImageView.setImageIcon(Icon.createWithResource(this, R.drawable.user_icon1));
        clearIconButton.setVisibility(View.INVISIBLE);
    }
    public void pickImage(View view){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        Log.i(tag, "start picking image");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK){
            newUserIconUri = data.getData();
            userIconImageView.setImageURI(newUserIconUri);
            Log.i(tag, "image picked and set");
            String type = newUserIconUri.toString();
            Snackbar.make(layout, type, Snackbar.LENGTH_SHORT).show();
            clearIconButton.setVisibility(View.VISIBLE);
            String path = data.getDataString();
            // testing
         /*   File file = new File(path);

            try {
                byte[] a = new byte[50];
                InputStream inputStream = new FileInputStream(file);
                inputStream.read(a);
                Toast.makeText(this, a.toString(), Toast.LENGTH_SHORT).show();
            } catch (FileNotFoundException e) {
                Toast.makeText(this, "file not found", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                Toast.makeText(this, "io exception", Toast.LENGTH_SHORT).show();
            }
         */
            Toast.makeText(this, data.getData().getEncodedPath(), Toast.LENGTH_SHORT).show();
        }
    }

    private void setChangeable(boolean isChangeable){
        Log.i(tag,"Set View element changeable to: "+isChangeable);
        usernameEditText.setEnabled(isChangeable);
        passwordEditText.setEnabled(isChangeable);
        createAccountCheckBox.setEnabled(isChangeable);
        serverIpEditText.setEnabled(isChangeable);
        setIconButton.setClickable(isChangeable);
        signInButton.setClickable(isChangeable);
    }

    public void signInClick(View view){
        Log.i(tag,"Clicked on sign in button");
        if(!StringTools.checkIP(serverIpEditText.getText().toString()) || !StringTools.checkUsername(usernameEditText.getText().toString()) || !StringTools.checkPass(passwordEditText.getText().toString())){
//            Toast.makeText(this, getString(R.string.signInBtnError), Toast.LENGTH_SHORT).show();
            Snackbar.make(layout, R.string.signInBtnError, Snackbar.LENGTH_LONG).show();
            Log.e(tag, "Error in username or password or server ip EditText");
        }
        else {
            if(AppSocket.getInstance().isConnected()){
                setChangeable(false);
                setConnected(true);
            }
            else {
                setChangeable(false);
                String serverStr = serverIpEditText.getText().toString();
                Pair<String, Integer> result = StringTools.getIpAndPortFromStr(serverStr);
                String serverIP = result.first;
                int serverPort = result.second;
                if (serverPort == 0) {
                    AppSocket.getInstance().setServerProperties(serverIP);
                } else {
                    AppSocket.getInstance().setServerProperties(serverIP, serverPort);
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        setConnected(AppSocket.getInstance().tryConnect());
                    }
                }).start();
                Log.i(tag, "Making connection to (" + serverIP + ", " + serverPort + ")");
            }
        }
    }

    private void setConnected(boolean isConnected){
        if(isConnected){
            Log.i(tag, "Server is online");
            String username = usernameEditText.getText().toString();
            String password = passwordEditText.getText().toString();
            if(isCreatingAccount){
                // todo use create account
            }
            else {
                RequestManager.getInstance().addToSending(
                    new LoginRequest(username, password)
                );
            }
        }
        else {
            Log.e(tag, "Server is offline");
            setChangeable(true);
            Snackbar.make(layout, R.string.cannotConnectSnackBarMessage, Snackbar.LENGTH_LONG).show();
        }
    }

    public void setSignInResult(boolean isLoggedIn, String userUuid){
        if(!isLoggedIn){ // when failed to sign in
            setChangeable(true);
            Snackbar.make(layout, R.string.signInFailedSnackBarMessage, Snackbar.LENGTH_LONG).show();
        }
        else {
            saveServerData();
            saveLoginData(userUuid);
            goToMainActivity();
        }
    }

    private void saveServerData(){
            SharedPreferences.Editor shEditor = getSharedPreferences("server_data", MODE_PRIVATE).edit();
            shEditor.putBoolean("logged_in", true);
            shEditor.putString("server_ip", AppSocket.getInstance().getServerIP());
            shEditor.putInt("server_port", AppSocket.getInstance().getPort());
            shEditor.commit();
        }

    private void saveLoginData(String userUuid){
        // todo save login data to sh pref
    }

    public void signInResult(boolean result, String userID){
        Log.i(tag,"get result =" + result + (userID==null?"":", id= "+userID));
        if(!result){
            setChangeable(true);
            return;
        }
        SimpleData simpleData = SimpleData.getInstance();
        String ip = simpleData.serverIp;
        int port = simpleData.serverPort;
        String username = simpleData.myUserName;
        String password = simpleData.password;
        saveSignInData(userID, port,username, ip,password);
        simpleData.myID = userID;
        if(newUserIconUri != null){
            Log.i(tag,"making initial icon request");
            new ChangeProfileIconRequest(userID,newUserIconUri.getPath());
        }
        User user = new User(username,userID,false,new Date());
        DatabaseManager.getInstance().addUser(user);
        goToMainActivity();
    }


    private void saveSignInData(String myID,int port, String myUsername,String serverIp,String password){
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("password",password);
        editor.putBoolean("Signed",true);
        editor.putString("serverIp",serverIp);
        editor.putString("myUsername",myUsername);
        editor.putString("id",myID);
        editor.putInt("port",port);
        editor.commit();
        User user = new User(myUsername,myID,false,new Date());
        DatabaseManager.getInstance().addUser(user);
        return;
    }

    private void makeSimpleDataClass(String ip, int port, String myUsername, String password){
        new SimpleData(ip,port,myUsername,password);
    }

    private void goToMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
