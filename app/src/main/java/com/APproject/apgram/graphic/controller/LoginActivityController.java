package com.APproject.apgram.graphic.controller;

import com.APproject.apgram.graphic.activity.LoginActivity;

import java.net.PortUnreachableException;

public class LoginActivityController{
    private static LoginActivityController singletonObject;

    public static LoginActivityController getInstance(){
        if(singletonObject == null)
            new LoginActivityController();
        return singletonObject;
    }

    LoginActivity loginActivity;
    boolean isWorking;

    private LoginActivityController(){
        singletonObject = this;
        isWorking = false;
    }

    public void setLoginActivity(LoginActivity loginActivity){
        this.loginActivity = loginActivity;
        isWorking = true;
    }

    public void setLoginActivityDone(){
        isWorking = false;
    }

    public void setSignInFailed(){
        if(isWorking) {
            loginActivity.setSignInResult(false, null);
        }
    }

    public void setSignedIn(String userUuid){
        if(isWorking) {
            loginActivity.setSignInResult(true, userUuid);
        }
    }
}
