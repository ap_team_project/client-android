package com.APproject.apgram.network;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

public class AppSocket implements Runnable {
    private static AppSocket singletonObject;
    public static AppSocket getInstance(){
        if(singletonObject != null)
            return singletonObject;
        else
            return new AppSocket(null, 0);
    }
    private final static String TAG = "APP-SOCKET";
    private final static int DEFAULT_PORT = 12345;
    private final static short LENGTH_BYTES_COUNT = 4;

    private final static String FIRST_SENDING_DATA = "APGRAM CLIENT V2";
    private final static String FIRST_RECEIVING_DATA = "APGRAM SERVER V2";


    private Socket socket;
    private InputStream in;
    private OutputStream out;


    private String serverIP;
    private int port;
    private  boolean isConnected = false;

    public AppSocket(String serverIP, int port) {
        this.serverIP = serverIP;
        this.port = port;
        singletonObject = this;
    }
    public AppSocket(String serverIP){
        this.serverIP = serverIP;
        this.port = DEFAULT_PORT;
        singletonObject = this;
    }

    public String getServerIP() {
        return serverIP;
    }

    public int getPort() {
        return port;
    }

    public boolean tryConnect(){
        if(serverIP == null)
            return false;
        if(isConnected)
            return true;
        try {
            socket = new Socket(serverIP, port);
            in = socket.getInputStream();
            out = socket.getOutputStream();
            send(FIRST_SENDING_DATA);
            if(FIRST_RECEIVING_DATA.equals(get())){
                isConnected = true;
                new Thread(this).start();
                return true;
            }
            else {
                socket.close();
                Log.i(TAG, "First time receive data is not correct");
                return false;
            }
        } catch (IOException e) {
            return false;
        }

    }

    public boolean isConnected(){
        return isConnected;
    }

    public void setServerProperties(String serverIP){
        setServerProperties(serverIP, DEFAULT_PORT);
    }

    public void setServerProperties(String serverIP, int port){
        this.serverIP = serverIP;
        this.port = port;
    }


    public boolean send(String data){
        ByteBuffer buffer = ByteBuffer.allocate(LENGTH_BYTES_COUNT+data.length());
        buffer.putInt(data.length());
        buffer.put(data.getBytes());    // here can change charset
        try {
            out.write(buffer.array());
            out.flush();
            Log.i(TAG, "Sent : "+data);
            return true;
        }catch (IOException e){
            return false;
        }
    }

    private final String get() throws IOException {
        byte[] lengthBytes = new byte[LENGTH_BYTES_COUNT];
        in.read(lengthBytes);
        int length = ByteBuffer.wrap(lengthBytes).getInt();
        byte[] dataByte = new byte[length];
        in.read(dataByte);
        return new String(dataByte);    // here can change charset
    }

    @Override
    public void run() {
        while (true){
            try {
                String inputStr = get();
                Response response = Response.parseResponse(inputStr);
                RequestManager.getInstance().setGottenResponse(response);
            } catch (IOException e) {
                Log.e(TAG, "Error in receiving data from input stream");
            } catch (Exception e) {
                Log.e(TAG, "Error in parsing received data to Response");
            }
        }
    }
}
