package com.APproject.apgram.network;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public abstract class Request {
    public static final String TAG = "REQUEST";

    String uuid;
    public Request(int requestType){
        uuid = UUID.randomUUID().toString();
        this.requestType = requestType;
    }

    private int requestType;

    public abstract JSONObject createRequestSendingData() throws JSONException; // It should create the JSONObject with the variable have

    public final JSONObject createRequestJsonObject() throws JSONException {
        try {
            JSONObject jsonObject = createRequestSendingData();
            JSONObject json = new JSONObject();
            json = json.put("uuid", uuid).put("request_type", requestType).put("data", jsonObject);
            return json;
        } catch (JSONException e) {
            Log.e(TAG, "Error in create json object in the last layer");
            throw e;
        }
    }

    public final String getUuid(){
        return uuid;
    }

    public abstract void onFail(int failureType);

    // only use when want to make another request
    public abstract void onSuccess();   // may be some request need to send other requests for example CreateAccount request may need to send icon if accepted
}
