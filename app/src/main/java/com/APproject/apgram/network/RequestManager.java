package com.APproject.apgram.network;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestManager implements Runnable{
    private static RequestManager singletonObject;

    public static RequestManager getInstance(){
        if(singletonObject != null)
            return singletonObject;
        else
            return new RequestManager();
    }

    private List<Request> sending;
    private Map<String, Request> noResponseProcessDone;

    private RequestManager(){
        sending = new ArrayList<>();
        noResponseProcessDone = new HashMap<>();
        singletonObject = this;
        new Thread(this).start();
    }

    public boolean addToSending(Request request){
        sending.add(request);
        return true;
    }

    public boolean setGottenResponse(Response response){
        Request request = noResponseProcessDone.remove(response.getUuid());
        response.setRequest(request);
        response.start();
        return true;
    }


    @Override
    public void run() {
        while (true){
            if(sending.size() == 0) {   // if sending queue is empty sleep and skip the process
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
            }
            else{
                int size = sending.size();
                for(int i = 0; i < size; i++){
                    Request r = sending.remove(0);
                    try {
                        JSONObject jsonObject = r.createRequestJsonObject();
                        String jsonString = jsonObject.toString();
                        AppSocket.getInstance().send(jsonString);
                        noResponseProcessDone.put(r.getUuid(), r);
                    } catch (JSONException e){continue;}
                }
            }
        }
    }
}
