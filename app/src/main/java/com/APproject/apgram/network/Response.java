package com.APproject.apgram.network;

import com.APproject.apgram.network.input.LoginResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public abstract class Response extends Thread {
    String uuid;
    Request request;
    boolean isSuccess;
    int failureType;
    protected JSONObject jsonData;    // todo set data type

    public Response(){}

    private void setData(String uuid, String data, boolean isSuccess, int failureType) throws JSONException {
        this.uuid = uuid;
        this.isSuccess = isSuccess;
        this.failureType = failureType;
        this.jsonData = new JSONObject(data);
    }

    public String getUuid(){
        return uuid;
    }

    public void setRequest(Request request){
        this.request = request;
    }

    public abstract void parseData();   // it should parse data from data variable to needed variables

    public abstract void handleResponse();

    @Override
    public final void run() {
        if(!isSuccess){
            this.request.onFail(this.failureType);
            return;
        }
        if(this.request != null)
            this.request.onSuccess();
        parseData();
        handleResponse();
    }



    /*
    1 : SIGN_IN_RESPONSE

     */
    private static Response getResponse(int responseType){
        if(responseType == 1)
            return new LoginResponse();

        // todo add other response with type here
        return null;
    }
    public static final Response parseResponse(String data) throws Exception {   // it will create Response object using the given data variable
        JSONObject jsonObject = new JSONObject(data);
        int responseType = jsonObject.getInt("response_type");
        String uuid = jsonObject.getString("uuid");
        boolean isSuccess = jsonObject.getBoolean("is_success");
        int failureType = jsonObject.getInt("failure_type");
        String responseData = jsonObject.getString("data");
        Response response = getResponse(responseType);
        if(response == null){
            throw new Exception("Not valid response type");
        }
        response.setData(uuid, responseData, isSuccess, failureType);
        return response;
    }
}
