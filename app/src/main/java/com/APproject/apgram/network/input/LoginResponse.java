package com.APproject.apgram.network.input;

import com.APproject.apgram.graphic.controller.LoginActivityController;
import com.APproject.apgram.network.Response;

import org.json.JSONException;

public class LoginResponse extends Response {
    String userUuid;

    @Override
    public void parseData() {
        try {
            userUuid = jsonData.getString("user_uuid");
        } catch (JSONException e) {
        }
    }

    @Override
    public void handleResponse() {
        LoginActivityController.getInstance().setSignedIn(userUuid);
        // todo set signed in for main activity
    }
}
