package com.APproject.apgram.network.output;

import com.APproject.apgram.network.Request;

import org.json.JSONException;
import org.json.JSONObject;

public class CreateAccountRequest extends Request {
    private static final Integer REQUEST_TYPE = 2;

    private String username;
    private String password;

    public CreateAccountRequest(String username, String password){
        super(REQUEST_TYPE);
        this.username = username;
        this.password = password;
    }


    @Override
    public JSONObject createRequestSendingData() throws JSONException {
        JSONObject json = new JSONObject();
        json = json.put("username", username).put("password", password);
        return json;
    }

    @Override
    public void onFail(int failureType) {
        // todo write onFail for create account request
    }

    @Override
    public void onSuccess() {
        // todo implement : if sould have icon
    }
}
