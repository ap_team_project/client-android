package com.APproject.apgram.network.output;

import com.APproject.apgram.graphic.controller.LoginActivityController;
import com.APproject.apgram.network.Request;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginRequest extends Request {
    private static final Integer REQUEST_TYPE = 1;

    String username;
    String password;

    public LoginRequest(String username, String password) {
        super(REQUEST_TYPE);
        this.username = username;
        this.password = password;
    }

    @Override
    public JSONObject createRequestSendingData() throws JSONException {
        JSONObject json = new JSONObject();
        json = json.put("username", username).put("password", password);
        return json;
    }

    @Override
    public void onFail(int failureType) {
        LoginActivityController.getInstance().setSignInFailed();
        // todo connect to main activity
    }

    @Override
    public void onSuccess() {
        return; // nothing work
    }
}
