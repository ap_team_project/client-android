package com.APproject.apgram.tools;

import android.util.Pair;
import android.util.Printer;

public class StringTools {
    private static final String CHECK_USERNAME_REGEX = "^[a-zA-Z0-9_]*$";
    private static final String CHECK_PASSWORD_REGEX = "^[a-zA-Z0-9_,.?!@#$%^&*(){}]*$";
    private static final String CHECK_IP_NO_PORT_REGEX = "\\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\\.|$)){4}\\b";

    private static boolean checkRegex(String str, String regex){
        if(str.length() == 0)
            return false;
        if(str.matches(regex))
            return true;
        return false;
    }

    public static boolean checkUsername(String str){
        return checkRegex(str, CHECK_USERNAME_REGEX);
    }
    public static boolean checkPass(String str){
        return checkRegex(str, CHECK_PASSWORD_REGEX);
    }

    public static boolean checkIP(String str){
        if(checkRegex(str, CHECK_IP_NO_PORT_REGEX))
            return true;
        else if(str.contains(":")){
            int index = str.indexOf(":");
            try {
                String portSection = str.substring(index + 1);
                int port = Integer.parseInt(portSection);
                if(port > 0 && port <= 65535)
                    return true;
            }catch (Exception e){
                return false;
            }
        }
        return false;
    }

    public static Pair<String, Integer> getIpAndPortFromStr(String str){
        if(!str.contains(":")){
            return new Pair<>(str, 0);
        }
        else{
            int index = str.indexOf(":");
            String ip = str.substring(0, index);
            int port = Integer.parseInt(str.substring(index + 1));
            return new Pair<>(ip, port);
        }
    }
}
